# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.3] - 2022-05-02

### Added

- Cookie consent ([#53](https://gitlab.com/operator-ict/golemio/weby/covid-19-prague/sampling-points-frontend/-/issues/53))

## [1.1.2] - 2022-03-22

### Fixed

- Filter status string ([#56](https://gitlab.com/operator-ict/golemio/weby/covid-19-prague/sampling-points-frontend/-/issues/56))

## [1.1.1] - 2022-03-09

### Fixed

- Location filter ([#55](https://gitlab.com/operator-ict/golemio/weby/covid-19-prague/sampling-points-frontend/-/issues/55))
- Calendar translation ([#54](https://gitlab.com/operator-ict/golemio/weby/covid-19-prague/sampling-points-frontend/-/issues/54))

## [1.1.0] - 2022-02-14

### Added

- Add new filters ([#51](https://gitlab.com/operator-ict/golemio/weby/covid-19-prague/sampling-points-frontend/-/issues/51))

### Changed

- Change design ([#51](https://gitlab.com/operator-ict/golemio/weby/covid-19-prague/sampling-points-frontend/-/issues/51))
- Change filtering strategy (using API for filternig) ([#51](https://gitlab.com/operator-ict/golemio/weby/covid-19-prague/sampling-points-frontend/-/issues/51))
