FROM bitnami/node:16.13.0 AS builder
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN export PUBLIC_URL="/"; \
    export NODE_PATH=./src; \
    yarn build


FROM bitnami/node:16.13.0-prod
WORKDIR /app
RUN yarn global add serve

COPY --from=builder /app/build .

# Create a non-root user
RUN groupadd --system nonroot &&\
    useradd --system --base-dir /app --uid 1001 --gid nonroot nonroot && \
    chown -R nonroot /app
USER nonroot

EXPOSE 3000
CMD ["serve","-s", "-l", "3000"]
