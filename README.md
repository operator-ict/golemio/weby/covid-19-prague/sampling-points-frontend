# Sampling points COVID-19

This website shows overview and details of suitable covid 19 sampling point services based on user defined filters.

## Czech description

Jde o jednoduchou a rychlou navigaci na místa kde se provádí testovaní na covid 19. Aplikace poskytuje uživatelům informace o všech pražských odběrových místech uceleně na jednom místě a usnadňuje nalezení vhodného odběrového místa pomocí informace o nejbližším volném termínu, ceny testu, rozlišení přijmu na samoplátce a osoby se žádankou od doktora, otevírací doby a nabídky všech variant služeb na daném odběrovém místě. Některá odběrová místa totiž nabízí možnost expresního testu, nebo například testování z auta (drive-in). V plánu je rozšíření o další informace například o odhad čekací doby ve frontě na místech bez rezervačního systému, dostupnost certifikátu o testu na daném místě, nebo vyhledání nejbližšího místa dle vaší aktuální polohy.

## Environmental variables

`PUBLIC_URL` - should be set to subdirectory where the app will be deployed

## Create react app

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn generate-api`

Generates api client from api.yml file.

NOTE: If you are on Windows using VS Code and you get the message

`Error: 'java' is not recognized as an internal or external command`

In the PowerShell terminal, run this command: `$env:Path += ";C:\Program Files\Java\jdk-17.0.1\bin"` where instead of `jdk-17.0.1` you put your installed JDK version.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
