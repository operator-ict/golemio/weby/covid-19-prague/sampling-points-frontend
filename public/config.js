// config map - this file gets replaced by env config during deployment
window.config = {
  API_URL: 'https://covid-praha.rabin.golemio.cz/v1/covid-odberova-mista-api',
  DATA_DOMAIN: 'localhost:3000',
  MAPBOX_TOKEN: 'FillApiKey',
  EMAIL: 'golemio@operatorict.cz',
  GA_CODE: 'UA-179230069-2',
  GA_CODE_COOKIELESS: 'UA-179230069-4',
  // delay in seconds before displaying old data notification
  DATA_REFRESH_WARNING_DELAY: 180,
};
