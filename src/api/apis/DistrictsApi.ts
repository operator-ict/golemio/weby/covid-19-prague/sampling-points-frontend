/* tslint:disable */
/* eslint-disable */
/**
 * Golemio COVID-19 Prague API
 * API for multiple reservation system for samling points of COVID19
 *
 * The version of the OpenAPI document: 1.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


import * as runtime from '../runtime';
import {
    District,
    DistrictFromJSON,
    DistrictToJSON,
} from '../models';

/**
 * 
 */
export class DistrictsApi extends runtime.BaseAPI {

    /**
     * List of all Districts that can be used as values of `?district` query filter in the `/samplingpoints` endpoint.
     */
    async districtsRaw(): Promise<runtime.ApiResponse<Array<District>>> {
        const queryParameters: any = {};

        const headerParameters: runtime.HTTPHeaders = {};

        const response = await this.request({
            path: `/districts`,
            method: 'GET',
            headers: headerParameters,
            query: queryParameters,
        });

        return new runtime.JSONApiResponse(response, (jsonValue) => jsonValue.map(DistrictFromJSON));
    }

    /**
     * List of all Districts that can be used as values of `?district` query filter in the `/samplingpoints` endpoint.
     */
    async districts(): Promise<Array<District>> {
        const response = await this.districtsRaw();
        return await response.value();
    }

}
