/* tslint:disable */
/* eslint-disable */
/**
 * Golemio COVID-19 Prague API
 * API for multiple reservation system for samling points of COVID19
 *
 * The version of the OpenAPI document: 1.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    SamplingPointsServicesRsSlots,
    SamplingPointsServicesRsSlotsFromJSON,
    SamplingPointsServicesRsSlotsFromJSONTyped,
    SamplingPointsServicesRsSlotsToJSON,
    SamplingPointsServicesRsSlotsFuture,
    SamplingPointsServicesRsSlotsFutureFromJSON,
    SamplingPointsServicesRsSlotsFutureFromJSONTyped,
    SamplingPointsServicesRsSlotsFutureToJSON,
} from './';

/**
 * 
 * @export
 * @interface SamplingPointsServicesRs
 */
export interface SamplingPointsServicesRs {
    /**
     * 
     * @type {number}
     * @memberof SamplingPointsServicesRs
     */
    id: number;
    /**
     * 
     * @type {string}
     * @memberof SamplingPointsServicesRs
     */
    name: string;
    /**
     * 
     * @type {string}
     * @memberof SamplingPointsServicesRs
     */
    link: string;
    /**
     * 
     * @type {string}
     * @memberof SamplingPointsServicesRs
     */
    note: string;
    /**
     * 
     * @type {Date}
     * @memberof SamplingPointsServicesRs
     */
    updatedAt: Date;
    /**
     * 
     * @type {Array<SamplingPointsServicesRsSlots>}
     * @memberof SamplingPointsServicesRs
     */
    slots?: Array<SamplingPointsServicesRsSlots> | null;
    /**
     * 
     * @type {Array<SamplingPointsServicesRsSlotsFuture>}
     * @memberof SamplingPointsServicesRs
     */
    freeSlotsRange?: Array<SamplingPointsServicesRsSlotsFuture> | null;
}

export function SamplingPointsServicesRsFromJSON(json: any): SamplingPointsServicesRs {
    return SamplingPointsServicesRsFromJSONTyped(json, false);
}

export function SamplingPointsServicesRsFromJSONTyped(json: any, ignoreDiscriminator: boolean): SamplingPointsServicesRs {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': json['id'],
        'name': json['name'],
        'link': json['link'],
        'note': json['note'],
        'updatedAt': (new Date(json['updated_at'])),
        'slots': !exists(json, 'slots') ? undefined : (json['slots'] === null ? null : (json['slots'] as Array<any>).map(SamplingPointsServicesRsSlotsFromJSON)),
        'freeSlotsRange': !exists(json, 'free_slots_range') ? undefined : (json['free_slots_range'] === null ? null : (json['free_slots_range'] as Array<any>).map(SamplingPointsServicesRsSlotsFutureFromJSON)),
    };
}

export function SamplingPointsServicesRsToJSON(value?: SamplingPointsServicesRs | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'name': value.name,
        'link': value.link,
        'note': value.note,
        'updated_at': (value.updatedAt.toISOString()),
        'slots': value.slots === undefined ? undefined : (value.slots === null ? null : (value.slots as Array<any>).map(SamplingPointsServicesRsSlotsToJSON)),
        'free_slots_range': value.freeSlotsRange === undefined ? undefined : (value.freeSlotsRange === null ? null : (value.freeSlotsRange as Array<any>).map(SamplingPointsServicesRsSlotsFutureToJSON)),
    };
}


