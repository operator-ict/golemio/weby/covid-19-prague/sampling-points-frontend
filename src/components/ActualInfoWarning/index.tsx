import React from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import './actionInfoWarning.scss';

const ACTUAL_INFO_WARNING_TOOLTIP_VISIBILITY = 'ACTUAL_INFO_WARNING_TOOLTIP_VISIBILITY';
enum ActualInfoWarningTooltipVisibility {
  ON_HOVER = 'ON_HOVER',
  ALWAYS = 'ALWAYS',
}

export const ActualInfoWarning: React.FC = () => {
  const { t } = useTranslation();
  const ref = React.useRef<HTMLElement>(null);

  const [actualInfoTooltipVisibility, setActualInfoTooltipVisibility] = React.useState<string>(
    localStorage.getItem(ACTUAL_INFO_WARNING_TOOLTIP_VISIBILITY)
      ? ActualInfoWarningTooltipVisibility.ON_HOVER
      : ActualInfoWarningTooltipVisibility.ALWAYS
  );

  const handleOutsideClick = React.useCallback((e: MouseEvent) => {
    if (ref?.current?.parentNode !== e.target && ref?.current !== e.target) {
      setActualInfoTooltipVisibility(ActualInfoWarningTooltipVisibility.ON_HOVER);
      localStorage.setItem(
        ACTUAL_INFO_WARNING_TOOLTIP_VISIBILITY,
        ActualInfoWarningTooltipVisibility.ON_HOVER
      );
      document.body.removeEventListener('click', handleOutsideClick);
    }
  }, []);

  React.useEffect(() => {
    document.body.addEventListener('click', handleOutsideClick);
  }, [handleOutsideClick]);

  return (
    <OverlayTrigger
      show={
        actualInfoTooltipVisibility === ActualInfoWarningTooltipVisibility.ON_HOVER
          ? undefined
          : true
      }
      overlay={
        <Tooltip id="">
          <span ref={ref} className="simple-tooltip">
            {t('actualInfoWarning')}
          </span>
        </Tooltip>
      }
    >
      <span className="action-info-warning">?</span>
    </OverlayTrigger>
  );
};
