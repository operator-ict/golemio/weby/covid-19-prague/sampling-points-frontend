import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';

export const BooleanToString: FC<{ value: boolean }> = ({ value }) => {
  const { t } = useTranslation();

  if (value) {
    return <>{t('yes')}</>;
  } else {
    return <>{t('no')}</>;
  }
};
