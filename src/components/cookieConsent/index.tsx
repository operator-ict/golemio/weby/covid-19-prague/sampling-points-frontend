import { FC, useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import Nav from 'react-bootstrap/Nav';

import 'vanilla-cookieconsent';
import './cookieConsent.scss';

export const CookieConsent: FC = () => {
  const { i18n } = useTranslation();
  const lng = i18n.language;
  const cc = useRef<any>(null);

  useEffect(() => {
    const win = window as Window &
      typeof globalThis & {
        initCookieConsent: () => any;
        CookieConsent: any;
      };

    if (win.CookieConsent) {
      win.CookieConsent.updateLanguage(lng);
    }
  }, [lng]);

  useEffect(() => {
    const win = window as Window &
      typeof globalThis & {
        initCookieConsent: () => any;
        CookieConsent: any;
      };

    if (!win.initCookieConsent) {
      return;
    }
    cc.current = win.initCookieConsent();
    win.CookieConsent = cc.current;

    if (win.config.GA_CODE) {
      // ga init and disable
      // ReactGA.initialize(win.config.GA_CODE, { debug: isDev });
      (win as any)[`ga-disable-${win.config.GA_CODE}`] = true;
    }

    cc.current.run({
      current_lang: lng,
      autoclear_cookies: true, // default: false
      cookie_name: 'covid-praha-cookieconsent', // default: 'cc_cookie'
      // remove_cookie_tables: true,

      gui_options: {
        consent_modal: {
          layout: 'bar', // box,cloud,bar
          position: 'bottom center', // bottom,middle,top + left,right,center
          transition: 'slide', // zoom,slide
        },
        settings_modal: {
          layout: 'box', // box,bar
          // position: 'left',                // right,left (available only if bar layout selected)
          transition: 'slide', // zoom,slide
        },
      },

      onAccept: (cookie: any) => {
        for (const level of cookie.level) {
          // analytics category
          if (level === 'analytics' && win.config.GA_CODE) {
            // ga enable
            // docs https://developers.google.com/analytics/devguides/collection/analyticsjs/user-opt-out
            (win as any)[`ga-disable-${win.config.GA_CODE}`] = false;
          }
        }
      },

      onChange: (cookie: any, changed_preferences: any) => {
        // If analytics category's status was changed
        if (changed_preferences.indexOf('analytics') > -1) {
          // If analytics category is disabled
          if (!cc.current.allowedCategory('analytics')) {
            // Disable ga
            (win as any)[`ga-disable-${win.config.GA_CODE}`] = true;
          } else {
            // Enable ga
            (win as any)[`ga-disable-${win.config.GA_CODE}`] = false;
          }
        }
      },

      languages: {
        cs: {
          consent_modal: {
            title: 'Používáme cookies',
            description:
              'Díky využití souborů cookies zjišťujeme, co je pro uživatele zajímavé. Analýza návštěvnosti nám pomáhá web neustále zlepšovat. Nepoužíváme cookies k marketingovým účelům, ani je nepředáváme nikomu dalšímu. <br><br> Dovolíte nám je takto používat?',
            primary_btn: {
              text: 'Povolit vše',
              role: 'accept_all', // 'accept_selected' or 'accept_all'
            },
            secondary_btn: {
              text: 'Nastavení cookies',
              role: 'c-settings', // 'settings' or 'accept_necessary'
            },
          },
          settings_modal: {
            title: 'Přizpůsobit nastavení cookies',
            save_settings_btn: 'Uložit nastavení',
            accept_all_btn: 'Povolit vše',
            reject_all_btn: 'Povolit nezbytné',
            close_btn_label: 'Zavřít',
            blocks: [
              {
                title: 'Nezbytně nutné cookies',
                description:
                  'Tyto cookies pomáhají, aby webová stránka byla použitelná a fungovala správně. Ve výchozím nastavení jsou povoleny a nelze je zakázat.',
                toggle: {
                  value: 'necessary',
                  enabled: true,
                  readonly: true, // cookie categories with readonly=true are all treated as "necessary cookies"
                },
              },
              {
                title: 'Statistika',
                description:
                  'Díky těmto cookies víme, kam u nás chodíte nejraději a máme statistiky o celkové návštěvnosti našich stránek.',
                toggle: {
                  value: 'analytics', // there are no default categories => you specify them
                  enabled: false,
                  readonly: false,
                },
              },
            ],
          },
        },
        en: {
          consent_modal: {
            title: 'We use cookies',
            description:
              'Thanks to the use of cookies, we find out what is interesting for users. Traffic analysis helps us to constantly improve the website. We do not use cookies for marketing purposes, we do not pass it on to anyone else. <br> <br> Will you let us use them like this?',
            primary_btn: {
              text: 'Accept all',
              role: 'accept_all', // 'accept_selected' or 'accept_all'
            },
            secondary_btn: {
              text: 'Cookie settings',
              role: 'c-settings', // 'settings' or 'accept_necessary'
            },
          },
          settings_modal: {
            title: 'Customize cookie settings',
            save_settings_btn: 'Save settings',
            accept_all_btn: 'Accept all',
            reject_all_btn: 'Accept necessary',
            close_btn_label: 'Close',
            blocks: [
              {
                title: 'Necessary cookies',
                description:
                  'These cookies help to make the website usable and work properly. By default, they are enabled and cannot be disabled.',
                toggle: {
                  value: 'necessary',
                  enabled: true,
                  readonly: true, // cookie categories with readonly=true are all treated as "necessary cookies"
                },
              },
              {
                title: 'Statistics',
                description:
                  'Thanks to these cookies, we know where you like to go with us the most and we have statistics on the total traffic to our website.',
                toggle: {
                  value: 'analytics', // there are no default categories => you specify them
                  enabled: false,
                  readonly: false,
                },
              },
            ],
          },
        },
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Nav.Link
      href="#"
      className="p-2"
      onClick={() => cc.current && cc.current.showSettings()}
      data-cc="c-settings"
    >
      Cookies
    </Nav.Link>
  );
};
