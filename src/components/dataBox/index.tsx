import React, { FC, useCallback, useMemo, useState, useEffect, useRef, useContext } from 'react';
import { Filters, MethodFilter } from 'types/filters';
import './dataBox.scss';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useTranslation } from 'react-i18next';
import { FilterResultEnum } from 'hooks/allFilters';
import Button from 'react-bootstrap/esm/Button';
import { SamplingPointsServiceQueueTypeEnum, SamplingPointsServiceRequestTypeEnum } from 'api';
import { DetailInfo, WeightedService } from 'types/data';
import { Detail } from 'components/detail/detail';
import { useSorters } from 'hooks/sorters';
import { useAnalytics } from 'hooks/analytics';
import { DataTableMap } from 'components/dataTableMap';
import { DataBoxTable } from 'components/dataBoxTable';
import { getSamplingPoint } from './utils';
import { SortColumn } from 'types/sorting';
import { DataContext } from 'state/dataState';

const filterSuitableServices = (pointIndex: number, services: WeightedService[]) => {
  return services
    .filter(
      (service) =>
        service.samplingPointsId === pointIndex &&
        service.filterResult === FilterResultEnum.ALL_PASSED
    )
    .reduce((acc: number[], suitableService) => {
      const newAcc = [...acc, suitableService.id];
      return newAcc;
    }, []);
};

export const DataBox: FC<{ filters: Filters; openFaq: () => void }> = ({ filters, openFaq }) => {
  const { hasError, isLoading, points, refreshData, services } = useContext(DataContext);
  const { t } = useTranslation();
  const [analytics] = useAnalytics();
  const [activeDetailInfo, setActiveDetailInfo] = useState<DetailInfo>({
    pointId: 0,
    serviceId: 0,
  });
  const [isDetailOpen, setDetailOpen] = useState(false);

  const [shouldShowMap, setShouldShowMap] = useState(false);

  useEffect(
    () => {
      analytics.changeDataView(shouldShowMap, true);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const changeShouldShowMap = useCallback(
    (newShouldShowMap: boolean) => {
      setShouldShowMap(newShouldShowMap);
      analytics.changeDataView(newShouldShowMap, false);
    },
    [analytics, setShouldShowMap]
  );

  const [shouldSortByColumn, setShouldSortByColumn] = useState<SortColumn>(
    SortColumn.WAIT_TIME_OR_CLOSEST_SLOT
  );
  const { sortByClosestSlot, sortByPrice, sortByWaitTime, sortByResultTimeHours } = useSorters();

  const showPriceColumn = useMemo(() => {
    return filters.serviceType === SamplingPointsServiceRequestTypeEnum.Selfpaid;
  }, [filters]);

  const showClosestSlotColumn = useMemo(() => {
    return filters.reservationType !== SamplingPointsServiceQueueTypeEnum.Queue;
  }, [filters]);

  const showResultTimeHours = useMemo(() => {
    return filters.serviceType !== SamplingPointsServiceRequestTypeEnum.Agtests;
  }, [filters]);

  const showExpectedWaitTime = useMemo(() => {
    return !showClosestSlotColumn;
  }, [showClosestSlotColumn]);

  const tableColumns = useMemo(
    () => ({
      showPriceColumn,
      showClosestSlotColumn,
      showExpectedWaitTime,
    }),
    [showClosestSlotColumn, showExpectedWaitTime, showPriceColumn]
  );

  const finalSortByColumn = useMemo(() => {
    if (shouldSortByColumn === SortColumn.PRICE && showPriceColumn) {
      return SortColumn.PRICE;
    } else if (
      shouldSortByColumn === SortColumn.WAIT_TIME_OR_CLOSEST_SLOT &&
      (showClosestSlotColumn || showExpectedWaitTime)
    ) {
      return SortColumn.WAIT_TIME_OR_CLOSEST_SLOT;
    } else {
      return SortColumn.RESULT_TIME_HOURS;
    }
  }, [shouldSortByColumn, showPriceColumn, showClosestSlotColumn, showExpectedWaitTime]);

  let defaultSortSettings = useRef(true);
  useEffect(() => {
    analytics.changeSort(finalSortByColumn, defaultSortSettings.current);
    defaultSortSettings.current = false;
  }, [finalSortByColumn, analytics]);

  const sortedData = useMemo(() => {
    if (!services) {
      return services;
    }

    const getSorter = (sortState: SortColumn) => {
      if (sortState === SortColumn.PRICE) {
        return sortByPrice;
      } else if (sortState === SortColumn.WAIT_TIME_OR_CLOSEST_SLOT) {
        return showClosestSlotColumn ? sortByClosestSlot : sortByWaitTime;
      } else {
        return sortByResultTimeHours;
      }
    };

    const sorter = getSorter(finalSortByColumn);

    const partialySortedData = services?.sort(sorter);

    return partialySortedData?.sort((a, b) => {
      const getPriority = (filterResult: FilterResultEnum) => {
        switch (filterResult) {
          case FilterResultEnum.ALL_PASSED:
            return 0;
          case FilterResultEnum.MAIN_PASSED:
            return 1;
          case FilterResultEnum.FAILED:
            return 2;
        }
      };
      const priorityScoreA = getPriority(a.filterResult);
      const priorityScoreB = getPriority(b.filterResult);

      return priorityScoreA - priorityScoreB;
    });
  }, [
    finalSortByColumn,
    services,
    sortByClosestSlot,
    sortByPrice,
    sortByWaitTime,
    sortByResultTimeHours,
    showClosestSlotColumn,
  ]);

  const hideDetail = useCallback(() => setDetailOpen(false), []);

  const [suitableActiveServices, setSuitableActiveServices] = useState<number[]>([]);
  const showDetail = useCallback(
    (detailInfo: DetailInfo) => {
      const suitableServicesIds = filterSuitableServices(detailInfo.pointId, sortedData || []);
      setSuitableActiveServices(suitableServicesIds || []);
      setActiveDetailInfo(detailInfo);
      setDetailOpen(true);
      analytics.openModal(detailInfo);
    },
    [sortedData, analytics]
  );

  const detailData = useMemo(() => {
    return getSamplingPoint(activeDetailInfo.pointId, points);
  }, [activeDetailInfo.pointId, points]);

  return (
    <>
      <Row>
        <Col>
          <div className="data-box">
            {hasError ? (
              <div className="text-center">
                <h3 className="text-danger">{t('errorMessage')}</h3>
                <Button disabled={isLoading} onClick={refreshData}>
                  {t('tryAgain')}
                </Button>
              </div>
            ) : (
              <>
                {shouldShowMap ? (
                  <DataTableMap
                    points={points}
                    sortedServices={sortedData}
                    filters={filters}
                    showDetail={showDetail}
                    columns={tableColumns}
                    district={filters.districtFilter}
                    openFaq={openFaq}
                    shouldShowMap={shouldShowMap}
                    setShouldShowMap={changeShouldShowMap}
                  />
                ) : (
                  <DataBoxTable
                    points={points}
                    sortedServices={sortedData}
                    filters={filters}
                    showDetail={showDetail}
                    columns={tableColumns}
                    openFaq={openFaq}
                    finalSortByColumn={finalSortByColumn}
                    shouldSortByColumn={shouldSortByColumn}
                    setShouldSortByColumn={setShouldSortByColumn}
                    showResultTimeHours={showResultTimeHours}
                    shouldShowMap={shouldShowMap}
                    setShouldShowMap={changeShouldShowMap}
                  />
                )}
                <Detail
                  selectedServiceId={activeDetailInfo.serviceId}
                  suitableServicesIds={suitableActiveServices}
                  show={isDetailOpen}
                  detailData={detailData}
                  hideDetail={hideDetail}
                  filters={filters}
                />
              </>
            )}
          </div>
        </Col>
      </Row>
    </>
  );
};
