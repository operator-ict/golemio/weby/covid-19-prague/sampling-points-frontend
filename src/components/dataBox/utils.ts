import { SamplingPointWithGeometry } from 'types/data';

export const getSamplingPoint = (
  id: number,
  points: SamplingPointWithGeometry[] | undefined
): SamplingPointWithGeometry | undefined => points?.find((point) => point.id === id);
