import React, { Dispatch, FC, Fragment, ReactNode, SetStateAction } from 'react';
import { Button, Col, Row, Table } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import moment from 'moment-timezone';
import { SortDownAlt } from 'react-bootstrap-icons';
import { FilterResultEnum } from 'hooks/allFilters';
import { ServiceIcons } from 'components/serviceIcons';
import { OpeningHours } from 'components/openingHours';
import { useTimeFromNow } from 'hooks/timeFromNow';
import { SimpleTooltip } from 'components/simpleTooltip';
import { SlotsTooltipContent } from 'components/slotsTooltipContent';
import { Filters } from 'types/filters';
import { DetailInfo, SamplingPointWithGeometry, WeightedService } from 'types/data';
import { getSamplingPoint } from 'components/dataBox/utils';
import { WaitTimeStateEnum } from 'utils/waitTime';
import { DisplaySwitch } from 'components/displaySwitch';
import { SortColumn, TableColumns } from 'types/sorting';
import './dataBoxTable.scss';
import { useSubHeading } from 'hooks/subHeading';
import { Icon } from 'components/icon';

const ClickableTh: FC<{
  isClickable: boolean;
  action: () => void;
  children: ReactNode;
  suffixIfClickable: ReactNode;
}> = ({ isClickable, action, children, suffixIfClickable }) => {
  return (
    <th
      className={classNames({ 'data-box-table__clickable-cell': isClickable })}
      onClick={() => isClickable && action()}
    >
      {children}
      {isClickable && <> {suffixIfClickable}</>}
    </th>
  );
};

export const DataBoxTable: FC<{
  points: SamplingPointWithGeometry[];
  sortedServices: WeightedService[];
  filters: Filters;
  columns: TableColumns;
  finalSortByColumn: SortColumn;
  shouldSortByColumn: SortColumn;
  setShouldSortByColumn: Dispatch<SetStateAction<SortColumn>>;
  showResultTimeHours: boolean;
  shouldShowMap: boolean;
  setShouldShowMap: (newValue: boolean) => void;
  openFaq: () => void;
  showDetail: (detailInfo: DetailInfo) => void;
}> = ({
  points,
  sortedServices,
  filters,
  columns,
  finalSortByColumn,
  shouldSortByColumn,
  setShouldSortByColumn,
  showResultTimeHours,
  shouldShowMap,
  setShouldShowMap,
  openFaq,
  showDetail,
}) => {
  const { t } = useTranslation();
  const { getTimeFromNow, outdated } = useTimeFromNow(60);
  const { subHeading } = useSubHeading(filters, columns, finalSortByColumn);
  const { showPriceColumn, showClosestSlotColumn, showExpectedWaitTime } = columns;

  const sections = [
    {
      heading: t('databoxHeadingChosen'),
      subheading: subHeading,
      filterResults: [FilterResultEnum.ALL_PASSED],
      isMainTable: true,
    },
    {
      heading: t('databoxHeadingOther'),
      subheading: null,
      filterResults: [FilterResultEnum.MAIN_PASSED, FilterResultEnum.FAILED],
      isMainTable: false,
    },
  ];

  return (
    <>
      {sections.map(({ filterResults, heading, subheading, isMainTable }, index) => (
        <Fragment key={index}>
          <Row>
            <Col xs={12} md={isMainTable ? 8 : 9}>
              <h2 className="mt-3">{heading}</h2>
              {subheading && <p className="secondary">{subheading}</p>}
            </Col>
            {isMainTable && (
              <>
                <Col xs={12} md={4} className="toggle-buttons text-right mb-4">
                  <Button className="data-box__error-button" onClick={openFaq}>
                    <Icon iconName={'Warning'} color={'secondary'} size={12} />
                    {t('haveYouFoundError')}
                  </Button>
                  <DisplaySwitch
                    shouldShowMap={shouldShowMap}
                    setShouldShowMap={setShouldShowMap}
                  />
                </Col>
              </>
            )}
          </Row>
          <Table responsive={true} className="data-box-table">
            {isMainTable && (
              <colgroup>
                <col></col>
                <col></col>
                <col></col>
                <col></col>
                {showResultTimeHours && (
                  <col
                    className={classNames({
                      'data-box-table__highlight':
                        finalSortByColumn === SortColumn.RESULT_TIME_HOURS,
                    })}
                  />
                )}
                {showPriceColumn && (
                  <col
                    className={classNames({
                      'data-box-table__highlight': finalSortByColumn === SortColumn.PRICE,
                    })}
                  />
                )}
                {showClosestSlotColumn && (
                  <col
                    className={classNames({
                      'data-box-table__highlight':
                        finalSortByColumn === SortColumn.WAIT_TIME_OR_CLOSEST_SLOT,
                    })}
                  />
                )}
                {showExpectedWaitTime && (
                  <col
                    className={classNames({
                      'data-box-table__highlight':
                        finalSortByColumn === SortColumn.WAIT_TIME_OR_CLOSEST_SLOT,
                    })}
                  />
                )}
                <col></col>
              </colgroup>
            )}
            <thead>
              <tr>
                <th>{t('databoxTable.services')}</th>
                <th>{t('databoxTable.name')}</th>
                <th>{t('databoxTable.address')}</th>
                <th>{t('databoxTable.open')}</th>
                {showResultTimeHours && (
                  <ClickableTh
                    action={() => setShouldSortByColumn(SortColumn.RESULT_TIME_HOURS)}
                    suffixIfClickable={
                      <SortDownAlt
                        className={
                          shouldSortByColumn === SortColumn.RESULT_TIME_HOURS
                            ? 'text-primary'
                            : 'text-secondary'
                        }
                      />
                    }
                    isClickable={isMainTable}
                  >
                    {t('databoxTable.resultTimeHours')}
                  </ClickableTh>
                )}
                {showPriceColumn && (
                  <ClickableTh
                    action={() => setShouldSortByColumn(SortColumn.PRICE)}
                    suffixIfClickable={
                      <SortDownAlt
                        className={
                          shouldSortByColumn === SortColumn.PRICE
                            ? 'text-primary'
                            : 'text-secondary'
                        }
                      />
                    }
                    isClickable={isMainTable}
                  >
                    {t('databoxTable.price')}
                  </ClickableTh>
                )}
                {showExpectedWaitTime && (
                  <ClickableTh
                    action={() => setShouldSortByColumn(SortColumn.WAIT_TIME_OR_CLOSEST_SLOT)}
                    suffixIfClickable={
                      <SortDownAlt
                        className={
                          !showPriceColumn ||
                          shouldSortByColumn === SortColumn.WAIT_TIME_OR_CLOSEST_SLOT
                            ? 'text-primary'
                            : 'text-secondary'
                        }
                      />
                    }
                    isClickable={isMainTable}
                  >
                    {t('databoxTable.expectedWaitTime')}
                  </ClickableTh>
                )}
                {showClosestSlotColumn && (
                  <ClickableTh
                    action={() => setShouldSortByColumn(SortColumn.WAIT_TIME_OR_CLOSEST_SLOT)}
                    suffixIfClickable={
                      <SortDownAlt
                        className={
                          !showPriceColumn ||
                          shouldSortByColumn === SortColumn.WAIT_TIME_OR_CLOSEST_SLOT
                            ? 'text-primary'
                            : 'text-secondary'
                        }
                      />
                    }
                    isClickable={isMainTable}
                  >
                    {t('databoxTable.closestSlot')}
                  </ClickableTh>
                )}
                <th></th>
              </tr>
            </thead>
            <tbody>
              {sortedServices &&
                sortedServices
                  .filter((item) => filterResults.includes(item.filterResult))
                  .map((service) => {
                    const samplingPoint = getSamplingPoint(service.samplingPointsId, points);

                    return (
                      <tr key={service.id}>
                        <td>
                          <ServiceIcons
                            filters={filters}
                            service={service}
                            district={samplingPoint?.district}
                            districtName={samplingPoint?.districtName}
                          />
                        </td>
                        <td>
                          <p className="font-weight-bold">{samplingPoint?.name}</p>
                          <p className="text-secondary mt-1 small">{service.title}</p>
                        </td>
                        <td>{samplingPoint?.addressShort}</td>
                        <td>
                          {service.openingHours && (
                            <OpeningHours skipDayPrefix={true} hours={service.openingHours} />
                          )}
                        </td>
                        {showResultTimeHours && (
                          <td>
                            {service.resultTimeHours
                              ? t('hoursShort', {
                                  hours: service.resultTimeHours,
                                })
                              : t('unknown')}
                          </td>
                        )}
                        {showPriceColumn && (
                          <td>
                            {!!service.price &&
                              service.currency &&
                              `${service.price} ${t(service.currency)}`}
                          </td>
                        )}
                        {showExpectedWaitTime && (
                          <td>
                            {service.waitTimeState === WaitTimeStateEnum.Closed && t('closed')}
                            {service.waitTimeState === WaitTimeStateEnum.Unknown && t('unknown')}
                            {service.waitTimeState === WaitTimeStateEnum.Short && (
                              <>{t('shortQueueTime')}</>
                            )}
                            {service.waitTimeState === WaitTimeStateEnum.Specified && (
                              <SimpleTooltip
                                underline
                                id=""
                                text={
                                  service.queue?.timestampCreated
                                    ? t('updatedWhen', {
                                        when: getTimeFromNow(service.queue.timestampCreated),
                                      })
                                    : null
                                }
                              >
                                {t('waitTimeMin', {
                                  min: service.queue?.waitMin,
                                })}
                              </SimpleTooltip>
                            )}
                          </td>
                        )}
                        {showClosestSlotColumn && (
                          <td>
                            {(service.rs?.slots?.[0] && (
                              <SimpleTooltip
                                underline
                                id=""
                                text={
                                  <SlotsTooltipContent
                                    slots={service.rs.slots}
                                    updatedWhen={t('updatedWhen', {
                                      when: getTimeFromNow(service.rs.slots[0].updatedAt),
                                    })}
                                    outdated={outdated(service.rs.slots[0].updatedAt)}
                                  />
                                }
                                outdated={outdated(service.rs.slots[0].updatedAt)}
                              >{`${t('from')} ${moment(service.rs.slots[0].startAt).format(
                                'D. M. (H.mm)'
                              )}`}</SimpleTooltip>
                            )) ||
                              t('unknown')}
                          </td>
                        )}
                        <td>
                          <Button
                            id="openShowMoreButton"
                            variant={
                              service.filterResult === FilterResultEnum.ALL_PASSED
                                ? 'primary'
                                : 'secondary'
                            }
                            className="data-box-table__more-button"
                            onClick={() => {
                              showDetail({
                                pointId: service.samplingPointsId,
                                serviceId: service.id,
                              });
                              window.plausible('openShowMore: Click', {
                                props: {
                                  openShowMore: 'openShowMore',
                                },
                              });
                            }}
                          >
                            {t('more')}
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
            </tbody>
          </Table>
          {(!sortedServices ||
            (sortedServices &&
              sortedServices.filter((item) => filterResults.includes(item.filterResult)).length ===
                0)) && (
            <p className="text-center">
              <i>{t('servicesNotFound')}</i>
            </p>
          )}
        </Fragment>
      ))}
    </>
  );
};
