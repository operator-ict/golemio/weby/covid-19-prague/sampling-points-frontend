import React, { FC, useState, useMemo, useEffect } from 'react';
import { groupBy, prop } from 'ramda';
import ReactMapGL from 'react-map-gl';
import { DetailInfo, SamplingPointWithGeometry, WeightedService } from 'types/data';
import '../mapContainer/mapContainer.scss';
import { MapMarker } from 'components/mapMarker';
import { DistrictFilter, Filters } from 'types/filters';
import { useTranslation } from 'react-i18next';
import { Button, Col, Row } from 'react-bootstrap';
import { DisplaySwitch } from 'components/displaySwitch';
import { TableColumns } from 'types/sorting';
import { useSubHeading } from 'hooks/subHeading';
import { Icon } from 'components/icon';

const phaPosition = {
  latitude: 50.0627,
  longitude: 14.4742,
  zoom: 10,
};

const stcPosition = {
  latitude: 50.06133,
  longitude: 14.5185,
  zoom: 7.8,
};

const positionsMap = {
  [DistrictFilter.PHA]: phaPosition,
  [DistrictFilter.STC]: stcPosition,
  [DistrictFilter.ANY]: stcPosition,
};

export const DataTableMap: FC<{
  points: SamplingPointWithGeometry[];
  sortedServices: WeightedService[];
  showDetail: (detailInfo: DetailInfo) => void;
  columns: TableColumns;
  filters: Filters;
  district: DistrictFilter;
  openFaq: () => void;
  shouldShowMap: boolean;
  setShouldShowMap: (newValue: boolean) => void;
}> = ({
  points,
  sortedServices,
  showDetail,
  columns,
  filters,
  district,
  openFaq,
  shouldShowMap,
  setShouldShowMap,
}) => {
  const { t } = useTranslation();
  const [viewport, setViewport] = useState(phaPosition);

  useEffect(() => {
    setViewport(positionsMap[district]);
  }, [district]);

  const grouppedServices = useMemo(
    () => groupBy(prop('samplingPointsId'))(sortedServices),
    [sortedServices]
  );

  const { subHeading } = useSubHeading(filters, columns);

  if (!window.config.MAPBOX_TOKEN) {
    return null;
  }

  return (
    <>
      <Row className="mb-4">
        <Col xs={12} md={8}>
          <h2 className="mt-3">{t('databoxHeadingChosen')}</h2>
          {subHeading && <p className="secondary">{subHeading}</p>}
        </Col>
        <Col xs={12} md={4} className="toggle-buttons text-right">
          <Button className="data-box__error-button" onClick={openFaq}>
            <Icon iconName={'Warning'} color={'secondary'} size={12} />
            {t('haveYouFoundError')}
          </Button>
          <DisplaySwitch shouldShowMap={shouldShowMap} setShouldShowMap={setShouldShowMap} />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col>
          <ReactMapGL
            height={600}
            width="100%"
            {...viewport}
            mapboxApiAccessToken={window.config.MAPBOX_TOKEN}
            onViewportChange={(newViewport: typeof viewport) =>
              setViewport({
                latitude: newViewport.latitude,
                longitude: newViewport.longitude,
                zoom: newViewport.zoom,
              })
            }
            className="mb-3"
          >
            {points.map((samplingPoint) => {
              const pointServices = grouppedServices[samplingPoint.id];

              return (
                <MapMarker
                  key={samplingPoint.id}
                  point={samplingPoint}
                  services={pointServices}
                  showDetail={showDetail}
                  columns={columns}
                />
              );
            })}
          </ReactMapGL>
        </Col>
      </Row>
    </>
  );
};
