import * as React from 'react';

function SvgLeft(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 24 24" fill="none" {...props}>
      <path d="M16 20l-8-8 8-8" stroke="currentColor" strokeLinejoin="round" />
    </svg>
  );
}

export default SvgLeft;
