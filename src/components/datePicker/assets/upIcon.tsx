import * as React from 'react';

function SvgUp(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 24 24" fill="none" {...props}>
      <path d="M4 16l8-8 8 8" stroke="currentColor" strokeLinejoin="round" />
    </svg>
  );
}

export default SvgUp;
