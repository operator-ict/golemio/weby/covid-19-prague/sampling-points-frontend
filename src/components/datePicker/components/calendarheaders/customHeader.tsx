import React, { useCallback } from 'react';
import { format, getMonth, getYear } from 'date-fns';
import { Button, Col, Container, Row } from 'react-bootstrap';
import LeftArrow from '../../assets/leftIcon';
import RightArrow from '../../assets/rightIcon';

export interface HeaderProps {
  date: Date;
  decreaseMonth(): void;
  increaseMonth(): void;
  changeMonth(month: number): void;
  changeYear(month: number): void;
  prevMonthButtonDisabled: boolean;
  nextMonthButtonDisabled: boolean;
  decreaseYear(): void;
  increaseYear(): void;
  forceUpdate?(): void;
  prevYearButtonDisabled: boolean;
  nextYearButtonDisabled: boolean;
  locale: Locale;
  showMonthYearDropdown?: boolean;
  DropdownMonth?: any;
  monthDropdownRef?: any;
  DropdownYear?: any;
  yearDropdownRef?: any;
  minDate?: Date | null;
  maxDate?: Date | null;
  testIdSuffix?: string;
  translate: (code: string) => string;
}

const CustomHeader = ({
  date,
  decreaseMonth,
  increaseMonth,
  changeMonth,
  changeYear,
  prevMonthButtonDisabled,
  nextMonthButtonDisabled,
  locale,
  showMonthYearDropdown,
  DropdownMonth,
  monthDropdownRef,
  DropdownYear,
  yearDropdownRef,
  minDate,
  maxDate,
  testIdSuffix,
  translate,
}: HeaderProps) => {
  const fromYear = minDate ? getYear(minDate) : 1900;
  const toYear = maxDate ? getYear(maxDate) : 2100;
  const monthDisabledFrom = maxDate && getYear(maxDate) == getYear(date) ? getMonth(maxDate) : 11;
  const monthDisabledTo = minDate && getYear(minDate) == getYear(date) ? getMonth(minDate) : 0;

  const prevMonthTestId =
    'react-datepicker-previous-month-btn' + (Boolean(testIdSuffix) ? '_' + testIdSuffix : '');
  const nextMonthTestId =
    'react-datepicker-next-month-btn' + (Boolean(testIdSuffix) ? '_' + testIdSuffix : '');

  const changeMonthHandler = useCallback(
    (event: any | undefined) => {
      changeMonth(event?.target.value);
    },
    [changeMonth]
  );

  const changeYearHandler = useCallback(
    (event: any | undefined) => {
      changeYear(event?.target.value);
    },
    [changeYear]
  );

  const headerContent = showMonthYearDropdown ? (
    <>
      <DropdownMonth
        locale={locale}
        currentMonth={getMonth(date)}
        onChangeMonthHandler={changeMonthHandler}
        monthDropdownRef={monthDropdownRef}
        monthDisabledFrom={monthDisabledFrom}
        monthDisabledTo={monthDisabledTo}
        testIdSuffix={testIdSuffix}
      />
      <DropdownYear
        currentYear={getYear(date)}
        fromYear={fromYear}
        toYear={toYear}
        onChangeYearHandler={changeYearHandler}
        yearDropdownRef={yearDropdownRef}
        testIdSuffix={testIdSuffix}
      />
    </>
  ) : (
    <span
      className="pt-2 pb-2 ml-2 mr-2 text-capitalize datepicker__header--date"
      data-testid="rreact-datepicker-previous-month-value"
    >
      {format(date, 'LLLL yyyy', { locale })}
    </span>
  );

  return (
    <Container fluid className="pb-4">
      <Row>
        <Col className="d-flex justify-content-between align-items-center p-0">
          <Button
            aria-label={translate('prevMonth')}
            onClick={decreaseMonth}
            disabled={prevMonthButtonDisabled}
            variant="outline-primary"
            className="border-0 datepicker__header--control"
            data-testid={prevMonthTestId}
          >
            <LeftArrow />
          </Button>

          {headerContent}

          <Button
            aria-label={translate('nextMonth')}
            onClick={increaseMonth}
            disabled={nextMonthButtonDisabled}
            variant="outline-primary"
            className="border-0 datepicker__header--control"
            data-testid={nextMonthTestId}
          >
            <RightArrow />
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

export default CustomHeader;
