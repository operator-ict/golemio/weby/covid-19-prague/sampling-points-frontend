import React, { FC } from 'react';

export interface DropdownMonthProps {
  locale: Locale;
  currentMonth: number;
  onChangeMonthHandler(evt: any): void;
  monthDropdownRef?: any;
  monthDisabledFrom: number;
  monthDisabledTo: number;
  testIdSuffix?: string;
}

const DropdownMonth: FC<DropdownMonthProps> = ({
  locale,
  currentMonth,
  onChangeMonthHandler,
  monthDropdownRef,
  monthDisabledFrom,
  monthDisabledTo,
  testIdSuffix,
}) => {
  const months = [];

  for (let i = 0; i < 12; i++) months.push(locale.localize?.month(i));

  const dropdownTestId =
    'react-datepicker-month-select' + (Boolean(testIdSuffix) ? '_' + testIdSuffix : '');

  return (
    <select
      ref={monthDropdownRef}
      defaultValue={currentMonth}
      onChange={onChangeMonthHandler}
      className="datepickerDropdown"
      data-testid={dropdownTestId}
    >
      {months.map((name, index) => (
        <option
          key={index}
          disabled={
            (monthDisabledFrom && index > monthDisabledFrom) ||
            (monthDisabledTo && index < monthDisabledTo) ||
            false
          }
          className="datepickerOption"
          value={index}
        >
          {name}
        </option>
      ))}
    </select>
  );
};

export default DropdownMonth;
