import React, { FC } from 'react';

export interface DropdownYearProps {
  currentYear: number;
  fromYear: number;
  toYear: number;
  onChangeYearHandler(evt: any): void;
  yearDropdownRef?: any;
  testIdSuffix?: string;
}

const DropdownYear: FC<DropdownYearProps> = ({
  currentYear,
  fromYear,
  toYear,
  onChangeYearHandler,
  yearDropdownRef,
  testIdSuffix,
}) => {
  const years = [];

  for (let i = fromYear; i <= toYear; i++) years.push(i); //fix

  const dropdownTestId =
    'react-datepicker-year-select' + (Boolean(testIdSuffix) ? '_' + testIdSuffix : '');

  return (
    <select
      ref={yearDropdownRef}
      defaultValue={currentYear}
      onChange={onChangeYearHandler}
      className="datepickerDropdown"
      data-testid={dropdownTestId}
    >
      {years.map((value, index) => (
        <option key={index} value={value}>
          {value}
        </option>
      ))}
    </select>
  );
};

export default DropdownYear;
