import React, { FC } from 'react';
import { isToday } from 'date-fns';

interface DayContentProps {
  dayOfMonth: number;
  date?: Date;
  currentDayText: string;
}

const DayContent: FC<DayContentProps> = ({ dayOfMonth, date, currentDayText }) => {
  const testId = 'react-datepicker-daycontent-' + dayOfMonth;
  return (
    <div
      className="d-flex justify-content-center align-items-center p-0 h-100 flex-column"
      data-testid="react-datepicker-daycontent-wrapper"
    >
      {date && isToday(date) && (
        <div
          className="w-100 text-uppercase datepicker__day--today"
          data-testid="react-datepicker-daycontent-today-text"
        >
          {currentDayText}
        </div>
      )}
      <div className="w-100" data-testid={testId}>
        {dayOfMonth}
      </div>
    </div>
  );
};

export default DayContent;
