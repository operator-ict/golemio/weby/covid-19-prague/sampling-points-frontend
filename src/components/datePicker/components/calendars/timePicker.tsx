import React, { FC } from 'react';
import { Button, Col, Row, FormControl as RawFormControl } from 'react-bootstrap';
import SimpledDownArrow from '../../assets/downIcon';
import SimpleUpArrow from '../../assets/upIcon';

export interface TimePickerProps {
  hoursFrom: number;
  hoursTo: number;
  onIncreasedHourFrom(hours: number): void;
  onIncreasedHourTo(hours: number): void;
  onDecreasedHourFrom(hours: number): void;
  onDecreasedHourTo(hours: number): void;
  date: Date | number;
  onSetHoursFrom(hours: number): void;
  onSetHoursTo(hours: number): void;
  translate(key: string): string;
}

const TimePicker: FC<TimePickerProps> = ({
  hoursFrom,
  hoursTo,
  onIncreasedHourFrom,
  onIncreasedHourTo,
  onDecreasedHourFrom,
  onDecreasedHourTo,
  date,
  onSetHoursFrom,
  onSetHoursTo,
  translate,
}) => (
  <>
    <Row className="pt-3">
      <Col className="d-inline-flex justify-content-center flex-column text-center">
        <h5
          className="datepicker__timepicker-header"
          data-testid="react-datepicker-timepicker-header"
        >
          {translate('timePicker.header')}
        </h5>
        <p
          className="datepicker__timepicker-body pt-4"
          data-testid="react-datepicker-timepicker-body"
        >
          {translate('timePicker.body')}
        </p>
      </Col>
    </Row>
    <Row className="d-flex justify-content-center datepicker__timepicker-input-wrapper">
      <Col xs={1} className="d-flex flex-column align-items-center">
        <Button
          aria-label={translate('incHours')}
          variant="outline"
          className="btn-sm border-0"
          onClick={() => onIncreasedHourFrom(1)}
          data-testid="react-datepicker-timepicker-increaseHours"
        >
          <SimpleUpArrow />
        </Button>
        <RawFormControl
          type="number"
          value={hoursFrom}
          min="0"
          max="23"
          onChange={(e) => {
            onSetHoursFrom(Number(e.target.value));
          }}
          className="datepicker__timepicker-control"
          data-testid="react-datepicker-timepicker-hours-value"
          aria-label={translate('hours')}
        />
        <Button
          aria-label={translate('decHours')}
          variant="outline"
          className="btn-sm border-0"
          onClick={() => onDecreasedHourFrom(1)}
          data-testid="react-datepicker-timepicker-decreaseHours"
        >
          <SimpledDownArrow />
        </Button>
      </Col>
      <Col xs={4} className="d-flex align-items-center justify-content-center">
        <span
          className="font-weight-bold h5 mb-0"
          data-testid="react-datepicker-timepicker-divider"
        >
          {translate('until')}
        </span>
      </Col>
      <Col xs={1} className="d-flex flex-column align-items-center">
        <Button
          aria-label={translate('incMinutes')}
          variant="outline"
          className="btn-sm border-0"
          onClick={() => onIncreasedHourTo(1)}
          data-testid="react-datepicker-timepicker-increaseMinutes"
        >
          <SimpleUpArrow />
        </Button>
        <RawFormControl
          type="number"
          min="0"
          max="23"
          value={hoursTo}
          onChange={(e) => {
            onSetHoursTo(Number(e.target.value));
          }}
          className="datepicker__timepicker-control"
          data-testid="react-datepicker-timepicker-minutes-value"
          aria-label={translate('minutes')}
        />
        <Button
          aria-label={translate('decMinutes')}
          variant="outline"
          className="btn-sm border-0"
          onClick={() => onDecreasedHourTo(1)}
          data-testid="react-datepicker-timepicker-decreaseMinutes"
        >
          <SimpledDownArrow />
        </Button>
      </Col>
    </Row>
  </>
);

export default TimePicker;
