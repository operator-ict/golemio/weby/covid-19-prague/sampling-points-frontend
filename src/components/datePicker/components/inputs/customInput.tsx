import React, { ReactNode, useCallback } from 'react';
import { block } from 'bem-cn';
import { Button, Form, FormControlProps } from 'react-bootstrap';

import SimpledLeftArrow from '../../assets/leftIcon';
import SimpledRightArrow from '../../assets/rightIcon';
// import { Calendar } from "../../../Icons";

export interface InputWrapperProps {
  onDayIncrease(d: number): void;
  onDayDecrease(d: number): void;
  openCalendar(): void;
  isMinDate?: boolean;
  isMaxDate?: boolean;
  label: string;
  size?: 'lg' | 'md';
  onClick?: React.MouseEventHandler<HTMLElement>;
  isInvalid?: boolean;
  focused?: boolean;
  input?: any;
  testIdSuffix?: string;
  children?: ReactNode;
  inputId?: string;
  originValue: Date | null | undefined;
  translate: (code: string) => string;
}

const inputGroupClass = block('datepicker__input');

const CustomInput = React.forwardRef<HTMLInputElement, FormControlProps & InputWrapperProps>(
  (
    {
      value,
      onDayIncrease: onDayIncreaseInternal,
      onDayDecrease: onDayDecreaseInternal,
      openCalendar,
      label,
      size,
      isMinDate,
      isMaxDate,
      testIdSuffix,
      isInvalid,
      children,
      inputId,
      translate,
      originValue,
      ...props
    },
    ref
  ) => {
    const icon =
      size === 'lg' ? (
        <div className="form-group-icon">
          {/* <Calendar /> */}
          <p>icon</p>
        </div>
      ) : null;
    const prevDayTestId =
      'react-datepicker-previous-day-btn' + (Boolean(testIdSuffix) ? '_' + testIdSuffix : '');
    const nextDayTestId =
      'react-datepicker-next-day-btn' + (Boolean(testIdSuffix) ? '_' + testIdSuffix : '');
    const calendarShowTestId =
      'react-datepicker-open-calendar-btn' + (Boolean(testIdSuffix) ? '_' + testIdSuffix : '');
    const inputDateTestId =
      'react-datepicker-date-input' + (Boolean(testIdSuffix) ? '_' + testIdSuffix : '');
    const customProps = { ...props, id: inputId };
    const readOnly = Boolean(
      navigator.maxTouchPoints || 'ontouchstart' in document.documentElement
    );

    const onDayDecrease = useCallback(() => onDayDecreaseInternal(1), [onDayDecreaseInternal]);
    const onDayIncrease = useCallback(() => onDayIncreaseInternal(1), [onDayIncreaseInternal]);

    const appendix =
      size === 'lg' && originValue ? (
        <>
          <Button
            aria-label={translate('decDate')}
            disabled={isMinDate || isInvalid}
            variant="outline"
            size="sm"
            onClick={onDayDecrease}
            data-testid={prevDayTestId}
          >
            <SimpledLeftArrow />
          </Button>
          <Button
            aria-label={translate('incDate')}
            disabled={isMaxDate || isInvalid}
            variant="outline"
            size="sm"
            onClick={onDayIncrease}
            data-testid={nextDayTestId}
          >
            <SimpledRightArrow />
          </Button>
        </>
      ) : (
        <Button
          className="caret"
          variant="outline"
          size="sm"
          onClick={openCalendar}
          data-testid={calendarShowTestId}
          aria-label={translate('openCalendar')}
        />
      );

    return (
      <Form.Group
        className={inputGroupClass
          .mix(
            size === 'lg' ? 'form-group-lg' : '',
            isInvalid && 'is-invalid',
            props.disabled && 'disabled'
          )
          .toString()}
      >
        {icon}
        <Form.Label htmlFor={inputId}>{label}</Form.Label>
        <Form.Control
          {...customProps}
          ref={ref}
          type="text"
          value={originValue ? value : ''}
          size={size === 'lg' ? size : 'sm'}
          data-testid={inputDateTestId}
          readOnly={readOnly}
        />
        <div className="form-control-append">{appendix}</div>
        {children}
      </Form.Group>
    );
  }
);

export default CustomInput;
