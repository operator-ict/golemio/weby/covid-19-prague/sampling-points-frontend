import {
  addDays,
  getMonth,
  getYear,
  isSameDay,
  isToday,
  isTomorrow,
  max,
  min,
  subDays,
} from 'date-fns';
import React, {
  KeyboardEvent,
  FC,
  ReactNode,
  useCallback,
  useEffect,
  useRef,
  useState,
  useContext,
} from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import ReactDatePicker, { ReactDatePickerProps } from 'react-datepicker';
import CustomHeader from './components/calendarheaders/customHeader';
import CustomInput from './components/inputs/customInput';
import TimePicker, { TimePickerProps } from './components/calendars/timePicker';
import DayContent from './components/calendars/dayContent';
import DropdownMonth from './components/calendarheaders/dropdowns/dropdownMonth';
import DropdownYear from './components/calendarheaders/dropdowns/dropdownYear';
import { LanguageContext } from 'state/languageState';
import { cs, enGB } from 'date-fns/locale';

import './index.scss';

export interface DatePickerProps
  extends Pick<ReactDatePickerProps, 'locale' | 'minDate' | 'maxDate'> {
  translate(key: string): string;
  value: Date;
  isDateEmpty: boolean;
  times: { hoursFrom: number; hoursTo: number };
  showTimeOnly?: boolean;
  showDateOnly?: boolean;
  showMonthYearDropdown?: boolean;
  renderCustomTimePicker?: FC<TimePickerProps>;
  onChange(date: Date | null, hoursFrom: number, hoursTo: number): void;
  onClose?(): void;
  onCancel?(): void;
  label: string;
  size?: 'lg' | 'md';
  isInvalid?: boolean;
  testIdSuffix?: string;
  errorWrapper?: ReactNode;
  id?: string | number;
  reset?: unknown;
}

function fixDate(date: Date | null) {
  if (!date) return null;
  if (isToday(date)) {
    return new Date();
  } else {
    date.setHours(0, 0, 0);
  }

  return date;
}

function useManualDetection(setter: (...any: any[]) => void, detector: (value: boolean) => void) {
  return useCallback(
    (...args: Parameters<typeof setter>) => {
      setter(...args);
      detector(true);
    },
    [setter, detector]
  );
}

const triggerManualChangeKeyFrom = '0'.charCodeAt(0);
const triggerManualChangeKeyTo = '9'.charCodeAt(0);

const DatePicker: FC<Omit<ReactDatePickerProps, 'value' | 'onChange'> & DatePickerProps> = ({
  locale: Locale,
  showDateOnly,
  showTimeOnly,
  children,
  translate,
  renderCustomHeader,
  renderCustomTimePicker,
  value,
  isDateEmpty,
  times,
  onChange,
  onClose,
  onCancel,
  label,
  size,
  isInvalid,
  showMonthYearDropdown,
  minDate,
  maxDate,
  testIdSuffix,
  errorWrapper,
  id,
  selected,
  reset,
  ...rest
}) => {
  const { language } = useContext(LanguageContext);
  const locale = language === 'en' ? enGB : cs;
  const [isTimeManuallySet, setTimeManuallySet] = useState(false);
  const [isFocused, setFocused] = useState(false);

  useEffect(() => {
    setTimeManuallySet(false);
  }, [reset]);

  const datePickerRef = useRef<any | null>(null);
  const monthPickerRef = useRef<HTMLSelectElement | null>(null);
  const yearPickerRef = useRef<HTMLSelectElement | null>(null);

  const closeCalendar = useCallback(() => {
    onChange(value || selected, times.hoursFrom, times.hoursTo);
    onClose?.();
    datePickerRef.current?.setOpen(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [onChange, datePickerRef, onClose, value, selected]);

  const cancelSelection = useCallback(() => {
    onChange(null, times.hoursFrom, times.hoursTo);
    onCancel?.();
    datePickerRef.current?.setOpen(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [onChange, datePickerRef, onClose, value, selected]);

  const openCalendar = useCallback(() => {
    datePickerRef.current?.setOpen(true);
  }, [datePickerRef]);

  const updateDropDowns = useCallback(
    (date: Date) => {
      if (monthPickerRef.current) {
        monthPickerRef.current.selectedIndex = getMonth(date);
      }
      if (yearPickerRef.current) {
        yearPickerRef.current.value = getYear(date).toString();
      }
    },
    [monthPickerRef, yearPickerRef]
  );

  const onChangeFixed = useCallback(
    (date: Date | null) => {
      date = isTimeManuallySet ? date : fixDate(date);
      onChange(date, times.hoursFrom, times.hoursTo);
      date && updateDropDowns(date);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isTimeManuallySet, onChange]
  );

  const dayDecrease = useCallback(
    (days: number) => {
      const dates = minDate ? [subDays(value, days), minDate] : [subDays(value, days)];
      onChangeFixed(max(dates));
    },
    [onChangeFixed, minDate, value]
  );

  const dayIncrease = useCallback(
    (days: number) => {
      const dates = maxDate ? [addDays(value, days), maxDate] : [addDays(value, days)];
      onChangeFixed(min(dates));
    },
    [onChangeFixed, maxDate, value]
  );

  const renderCustomInput = (label: string, id?: string | undefined) => (
    <CustomInput
      inputId={id}
      label={label}
      size={size === 'lg' ? 'lg' : undefined}
      isInvalid={isInvalid}
      isMinDate={!!minDate && isSameDay(minDate, value)}
      isMaxDate={!!maxDate && isSameDay(maxDate, value)}
      onDayDecrease={dayDecrease}
      onDayIncrease={dayIncrease}
      openCalendar={openCalendar}
      testIdSuffix={testIdSuffix}
      originValue={value}
      translate={translate}
    >
      {errorWrapper}
    </CustomInput>
  );

  const customHeader =
    renderCustomHeader ||
    ((props) => (
      <CustomHeader
        {...props}
        locale={locale as any}
        showMonthYearDropdown={showMonthYearDropdown}
        DropdownMonth={DropdownMonth}
        DropdownYear={DropdownYear}
        monthDropdownRef={monthPickerRef}
        yearDropdownRef={yearPickerRef}
        minDate={minDate}
        maxDate={maxDate}
        testIdSuffix={testIdSuffix}
        translate={translate}
      />
    ));

  const onSetHoursFrom = useManualDetection(
    useCallback(
      (hours: number) => onChange(value, Math.min(hours, 23), times.hoursTo),
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [onChange, value]
    ),
    setTimeManuallySet
  );

  const onSetHoursTo = useManualDetection(
    useCallback(
      (hours: number) => onChange(value, times.hoursFrom, Math.min(hours, 23)),
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [onChange, value]
    ),
    setTimeManuallySet
  );

  const onIncreasedHourFrom = (hour: number) => {
    onSetHoursFrom(times.hoursFrom + hour);
  };

  const onDecreasedHourFrom = (hour: number) => {
    onSetHoursFrom(times.hoursFrom - hour);
  };

  const onIncreasedHourTo = (hour: number) => {
    onSetHoursTo(times.hoursTo + hour);
  };

  const onDecreasedHourTo = (hour: number) => {
    onSetHoursTo(times.hoursTo - hour);
  };

  const current = value || new Date();
  const TimePickerComponent = renderCustomTimePicker ?? TimePicker;
  const timePickerProps = {
    hoursFrom: times.hoursFrom,
    hoursTo: times.hoursTo,
    onIncreasedHourFrom,
    onIncreasedHourTo,
    onDecreasedHourFrom,
    onDecreasedHourTo,
    date: current,
    onSetHoursFrom,
    onSetHoursTo,
    translate,
  };

  const onFocus = useCallback(() => setFocused(true), [setFocused]);
  const onBlur = useCallback(() => setFocused(false), [setFocused]);

  const detectManualChange = useCallback(
    (event: KeyboardEvent<HTMLInputElement>) => {
      const keyCode = event.key.length === 1 ? event.key.charCodeAt(0) : 0;
      if (
        (keyCode >= triggerManualChangeKeyFrom && keyCode <= triggerManualChangeKeyTo) ||
        event.key === 'Backspace'
      ) {
        setTimeManuallySet(true);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [onChangeFixed, setTimeManuallySet]
  );

  let format = 'd. M. yyyy';

  if (!isFocused && !showTimeOnly) {
    const appendixFormat = 'PP';
    format = isToday(value)
      ? `'${translate('input.today')}' ${appendixFormat}`
      : isTomorrow(value)
      ? `'${translate('input.tomorrow')}' ${appendixFormat}`
      : format;
  }

  format = format + ` '${times.hoursFrom}:00' - '${times.hoursTo}:00'`;

  if (isDateEmpty) {
    format = `'${translate('input.select')}'`;
  }

  const customDayContent = useCallback(
    (dayOfMonth: number, date: Date) => (
      <DayContent dayOfMonth={dayOfMonth} date={date} currentDayText={translate('today')} />
    ),
    [translate]
  );

  const confirmTestId =
    'react-datepicker-confirm' + (Boolean(testIdSuffix) ? '_' + testIdSuffix : '');
  return (
    <div className={showDateOnly ? 'datepicker__date_only' : ''}>
      <ReactDatePicker
        adjustDateOnChange
        selected={current}
        onChange={onChangeFixed}
        // onKeyDown={detectManualChange}
        dateFormat={format}
        minDate={minDate}
        maxDate={maxDate}
        locale={locale}
        disabledDayAriaLabelPrefix={translate('dayDisabled')}
        chooseDayAriaLabelPrefix={translate('dayChoose')}
        shouldCloseOnSelect={false}
        ref={datePickerRef}
        showPopperArrow={false}
        onFocus={onFocus}
        onBlur={onBlur}
        onMonthChange={updateDropDowns}
        // popperModifiers={{
        //   preventOverflow: {
        //     enabled: true,
        //     escapeWithReference: false,
        //   },
        // }}
        customInput={renderCustomInput(label, id)}
        renderDayContents={customDayContent}
        renderCustomHeader={customHeader}
        {...rest}
      >
        {children ? (
          children
        ) : (
          <div className={!showDateOnly ? 'datepicker__body' : 'datepicker__column'}>
            <Container>
              <Row>
                <Col className="d-flex justify-content-center flex-column position-static">
                  {!showDateOnly && <TimePickerComponent {...timePickerProps} />}
                  <Row className="datepicker__body-confirm-row">
                    <Col
                      className={
                        !showDateOnly
                          ? 'position-absolute w-auto datepicker__body-confirm-button'
                          : 'datepicker__body-confirm-button'
                      }
                    >
                      <Button
                        variant="light"
                        className="pill"
                        onClick={cancelSelection}
                        data-testid={confirmTestId}
                      >
                        {translate('datePicker.cancel')}
                      </Button>
                      <Button
                        className="pill ml-3"
                        onClick={closeCalendar}
                        data-testid={confirmTestId}
                      >
                        {translate('datePicker.confirm')}
                      </Button>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Container>
          </div>
        )}
      </ReactDatePicker>
    </div>
  );
};

export default DatePicker;
