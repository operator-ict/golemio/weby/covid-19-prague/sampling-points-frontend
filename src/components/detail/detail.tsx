import React, { FC, useEffect, useState } from 'react';
import { Col, Modal, Row, Table } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { MapContainer } from 'components/mapContainer';
import { Filters } from 'types/filters';
import { GoogleMapsLink } from 'components/googleMapsLink';
import { ServiceAccordion } from './serviceAccordion';
import { Link } from 'components/link';
import { SamplingPointWithGeometry } from 'types/data';

export const Detail: FC<{
  detailData: SamplingPointWithGeometry | undefined;
  suitableServicesIds: number[];
  show: boolean;
  hideDetail: () => void;
  selectedServiceId: number;
  filters: Filters;
}> = ({ detailData, suitableServicesIds, show, hideDetail, selectedServiceId, filters }) => {
  const { t } = useTranslation();
  const [activeServiceId, setActiveServiceId] = useState<number | null>(selectedServiceId);

  useEffect(() => {
    setActiveServiceId(selectedServiceId);
  }, [selectedServiceId]);

  const handleSelect = (id: number | null) => {
    setActiveServiceId(id);
  };

  function formatURL(url: string) {
    return url.replace('http://', '').replace('https://', '');
  }

  const suitableServices =
    detailData?.services?.filter((service) => suitableServicesIds.includes(service.id)) || [];
  const hasSuitableServices = suitableServices.length !== 0;
  const otherServices =
    detailData?.services?.filter((service) => !suitableServicesIds.includes(service.id)) || [];
  return (
    <Modal show={show} onHide={hideDetail} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>
          <h2 className="text-primary">{detailData?.name}</h2>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Row>
          <Col lg={8} sm={12}>
            <div className="mb-4">
              <h3 className="text-secondary">{t('suitableServicesOfPoint')}</h3>
              {hasSuitableServices && (
                <ServiceAccordion
                  handleSelect={handleSelect}
                  district={detailData?.district}
                  districtName={detailData?.districtName}
                  paymentMethod={detailData?.paymentType || null}
                  services={suitableServices}
                  activeServiceId={activeServiceId}
                  filters={filters}
                />
              )}
              {!hasSuitableServices && <i>{t('noSuitableServices')}</i>}
            </div>
            {otherServices.length !== 0 && (
              <>
                <h3 className="text-secondary">{t('otherServicesOfPoint')}</h3>
                <ServiceAccordion
                  handleSelect={handleSelect}
                  district={detailData?.district}
                  districtName={detailData?.districtName}
                  paymentMethod={detailData?.paymentType || null}
                  services={otherServices}
                  filters={filters}
                  activeServiceId={activeServiceId}
                />
              </>
            )}
          </Col>
          <Col lg={4} md={12}>
            <h3 className="text-secondary">{t('contact')}</h3>
            <Row>
              <Col lg={12} md={8} sm={12}>
                {detailData && (
                  <Table className="detail__info-table" responsive={true} size="small">
                    <tbody>
                      <tr>
                        <th scope="row">{t(`detailContactTable.address`)}</th>
                        <td>
                          <GoogleMapsLink
                            lat={detailData.geometry.coordinates[1]}
                            lng={detailData.geometry.coordinates[0]}
                          >
                            {detailData.address}
                          </GoogleMapsLink>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">{t(`detailContactTable.website`)}</th>
                        <td>
                          {!!detailData?.providerLink && (
                            <Link
                              href={detailData.providerLink}
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              {formatURL(detailData.providerLink)}
                            </Link>
                          )}
                        </td>
                      </tr>
                      {!!detailData?.contact && (
                        <tr>
                          <th scope="row">{t(`detailContactTable.phone`)}</th>
                          <td>
                            <Link href={'tel:' + detailData.contact}>{detailData.contact}</Link>
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </Table>
                )}
              </Col>
              <Col lg={12} md={4} sm={12}>
                <div className="my-2">
                  {detailData && (
                    <MapContainer
                      initialPosition={{
                        latitude: detailData?.geometry.coordinates[1],
                        longitude: detailData?.geometry.coordinates[0],
                      }}
                      data={(detailData && [detailData]) || []}
                    />
                  )}
                </div>
              </Col>
              {detailData && detailData.locationDescription !== '' && (
                <Col sm={12}>
                  <h4 className="text-secondary">{t('locationDescription')}</h4>
                  <p>{detailData.locationDescription}</p>
                </Col>
              )}
            </Row>
          </Col>
        </Row>
      </Modal.Body>
    </Modal>
  );
};
