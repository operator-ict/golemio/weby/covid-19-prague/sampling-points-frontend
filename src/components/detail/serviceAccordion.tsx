import React, { FC, useEffect } from 'react';
import { Accordion, Button, Card, Col, Row, Table } from 'react-bootstrap';
import moment from 'moment-timezone';
import {
  SamplingPointsService,
  SamplingPointsServiceProcessTypeEnum,
  SamplingPointsServiceQueueTypeEnum,
  SamplingPointsServiceRequestTypeEnum,
} from 'api';
import classNames from 'classnames';
import { ServiceIcons } from 'components/serviceIcons';
import { useTranslation } from 'react-i18next';
import { BooleanToString } from 'components/booleanToString';
import { Filters } from 'types/filters';
import { OpeningHours } from 'components/openingHours';
import './serviceAccordion.scss';
import { SimpleTooltip } from 'components/simpleTooltip';
import { useTimeFromNow } from 'hooks/timeFromNow';
import { SlotsTooltipContent } from 'components/slotsTooltipContent';
import { useAnalytics } from 'hooks/analytics';
import { Link } from 'components/link';

export const ServiceAccordion: FC<{
  services?: SamplingPointsService[];
  district?: string | null;
  districtName?: string | null;
  paymentMethod: string | null;
  activeServiceId: number | null;
  filters: Filters;
  handleSelect: (id: number | null) => void;
}> = ({
  services,
  activeServiceId,
  district,
  districtName,
  filters,
  handleSelect,
  paymentMethod,
}) => {
  const { t } = useTranslation();
  const [analytics] = useAnalytics();

  const { getTimeFromNow, outdated } = useTimeFromNow(60);

  useEffect(() => {
    if (activeServiceId) {
      const pointId = services?.find((service) => service.id === activeServiceId)?.samplingPointsId;
      if (pointId) {
        analytics.changeService({ serviceId: activeServiceId, pointId: pointId });
      }
    }
  }, [analytics, activeServiceId, services]);

  return (
    <Accordion
      className="service-accordion"
      activeKey={activeServiceId ? activeServiceId.toString() : ''}
      onSelect={(idString) => handleSelect(idString ? +idString : null)}
    >
      {services?.map((service: SamplingPointsService) => (
        <Card key={service.id}>
          <Accordion.Toggle
            as={Card.Header}
            className={classNames('service-accordion__accordion-header', 'no-gutters', {
              active: service.id === activeServiceId,
            })}
            variant="link"
            eventKey={service.id.toString()}
          >
            <Col md={8} sm={6} className="font-weight-bold">
              {service.title || service.type}
            </Col>
            <Col md={4} sm={6}>
              <ServiceIcons
                colors={service.id === activeServiceId ? 'inverted' : 'normal'}
                service={service}
                district={district ? district : undefined}
                districtName={districtName ? districtName : undefined}
                filters={filters}
                isInDetail
              />
            </Col>
          </Accordion.Toggle>

          <Accordion.Collapse eventKey={service.id.toString()}>
            <Card.Body>
              <Row>
                <Col md={6} sm={12}>
                  <h3>{t('details')}</h3>
                  <Table className="service-accordion__info-table" responsive={true} size="small">
                    <tbody>
                      <tr>
                        <th scope="row">{t('filters.paymentType')}</th>
                        <td>
                          {(service.requestType === SamplingPointsServiceRequestTypeEnum.Selfpaid &&
                            t('filters.selfpaid')) ||
                            (service.requestType ===
                              SamplingPointsServiceRequestTypeEnum.Application &&
                              t('filters.withRequest')) ||
                            t('filters.agtests')}
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">{t('filters.method')}</th>
                        <td>
                          {service.processType === SamplingPointsServiceProcessTypeEnum.Drivein &&
                            t('filters.inCar')}
                          {service.processType === SamplingPointsServiceProcessTypeEnum.Walk &&
                            ((service.queueType === SamplingPointsServiceQueueTypeEnum.Queue &&
                              t('filters.walkWithoutReservation')) ||
                              t('filters.walkWithReservation'))}
                        </td>
                      </tr>
                      <tr>
                        <th scope="row">{t('filters.price')}</th>
                        <td>
                          {service.price} {t(service.currency)}
                        </td>
                      </tr>
                      {service.requestType === SamplingPointsServiceRequestTypeEnum.Selfpaid &&
                        paymentMethod && (
                          <tr>
                            <th scope="row">{t('paymentMethod')}</th>
                            <td>{paymentMethod}</td>
                          </tr>
                        )}
                      {service.requestType === SamplingPointsServiceRequestTypeEnum.Selfpaid && (
                        <tr>
                          <th scope="row">{t('certificate')}</th>
                          <td>
                            <BooleanToString value={service.certificate} />
                            {!!service.certificatePrice && (
                              <>
                                {' '}
                                ({service.certificatePrice} {t(service.certificateCurrency)})
                              </>
                            )}
                          </td>
                        </tr>
                      )}
                      <tr>
                        <th scope="row">{t('reservation')}</th>
                        <td>
                          <BooleanToString
                            value={
                              service.queueType === SamplingPointsServiceQueueTypeEnum.Reservation
                            }
                          />{' '}
                          {service.queueType === SamplingPointsServiceQueueTypeEnum.Reservation &&
                            !!service.rs?.slots?.length && (
                              <SimpleTooltip
                                underline
                                id=""
                                text={
                                  <SlotsTooltipContent
                                    slots={service.rs.slots}
                                    updatedWhen={t('updatedWhen', {
                                      when: getTimeFromNow(service.rs.slots[0].updatedAt),
                                    })}
                                    outdated={outdated(service.rs.slots[0].updatedAt)}
                                  />
                                }
                                outdated={outdated(service.rs.slots[0].updatedAt)}
                              >
                                <>
                                  ({t('from')}{' '}
                                  {moment(service.rs.slots[0].startAt).format('D. M. YYYY')})
                                </>
                              </SimpleTooltip>
                            )}
                        </td>
                      </tr>
                      {!!service.resultTimeHours && (
                        <tr>
                          <th scope="row">{t('resultTimeHours')}</th>
                          <td>{t('hoursShort', { hours: service.resultTimeHours })}</td>
                        </tr>
                      )}
                      {!!service.link && (
                        <tr>
                          <td></td>
                          <td>
                            <Link
                              id="openRSButton"
                              /* tracked from onClick */ ignoreAnalytics
                              onClick={() => {
                                analytics.book({
                                  url: service.link,
                                  serviceId: service.id,
                                  pointId: service.samplingPointsId,
                                });
                                window.plausible('openRS: Click', {
                                  props: {
                                    openRS: service.link,
                                  },
                                });
                              }}
                              href={service.link}
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              <Button>{t('openRS')}</Button>
                            </Link>
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </Table>
                </Col>
                <Col md={6} sm={12}>
                  <h3>{t('openingHours')}</h3>
                  <Table className="service-accordion__info-table" responsive={true} size="small">
                    <tbody>
                      {[0, 1, 2, 3, 4, 5, 6].map((dayIndex) => (
                        <tr key={dayIndex}>
                          <th scope="row">{t(`longDays.${dayIndex}`)}</th>
                          <td>
                            <OpeningHours
                              dayIndexProp={dayIndex}
                              hours={service.openingHours || []}
                              skipDayPrefix={true}
                            />
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </Col>
              </Row>
              {!!service.note && (
                <Row>
                  <Col>
                    <h3>{t('description')}</h3>
                    <p>{service.note}</p>
                    <hr />
                    <p>{t('serviceDisclaimer1')}</p>
                    <p>{t('serviceDisclaimer2')}</p>
                  </Col>
                </Row>
              )}
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      ))}
    </Accordion>
  );
};
