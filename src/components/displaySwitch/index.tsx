import React, { FC } from 'react';
import { ButtonGroup, ToggleButton } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import './displaySwitch.scss';

export const DisplaySwitch: FC<{
  shouldShowMap: boolean;
  setShouldShowMap: (newValue: boolean) => void;
}> = ({ shouldShowMap, setShouldShowMap }) => {
  const { t } = useTranslation();

  return (
    <ButtonGroup toggle className="display-switch">
      <ToggleButton
        type="radio"
        name="displayType"
        value="false"
        checked={!shouldShowMap}
        onChange={() => setShouldShowMap(false)}
        className="display-switch__button btn-small"
      >
        {t('displayType.list')}
      </ToggleButton>
      <ToggleButton
        type="radio"
        name="displayType"
        value="true"
        checked={shouldShowMap}
        onChange={() => setShouldShowMap(true)}
        className="display-switch__button btn-small"
      >
        {t('displayType.map')}
      </ToggleButton>
    </ButtonGroup>
  );
};
