import classNames from 'classnames';
import { Icon } from 'components/icon';
import React, { FC, useContext } from 'react';
import Select from 'react-select';
import { DataContext } from 'state/dataState';
import { Filters } from 'types/filters';
import './districtSelect.scss';

export const DistrictSelect: FC<{
  filters: Filters;
  onChange: (selectedItems: string[]) => void;
}> = ({ filters, onChange }) => {
  const { districts } = useContext(DataContext);

  const districtsOptions = districts.map((x) => {
    return {
      value: x.districtName,
      label: x.districtNameCs,
    };
  });

  const pragueItems = [...districtsOptions]
    .filter((x) => {
      return x.value.indexOf('Praha') > -1 && x.value !== 'PrahaVychod' && x.value !== 'PrahaZapad';
    })
    .sort((a, b) => {
      if (b.value === 'CelaPraha') {
        return 1;
      }
      if (a.value.length > b.value.length) {
        return 1;
      } else if (a.value.length < b.value.length) {
        return -1;
      }
      return 0;
    });

  const nonPragueItems = [...districtsOptions].filter((x) => {
    return !pragueItems.includes(x);
  });

  const groupedOptions = [
    {
      label: 'Praha',
      options: pragueItems,
    },
    {
      label: 'Střední Čechy',
      options: nonPragueItems,
    },
  ];

  const isActive = filters.districts.length > 0;

  return (
    <div
      className={classNames('district-select', 'filter-button', {
        'active btn-outline-primary': isActive,
      })}
    >
      <Icon iconName="Map" color={isActive ? 'primary' : 'secondary'} />
      <Select
        isMulti={true}
        isClearable={false}
        name="colors"
        options={groupedOptions}
        className="district-multi-select"
        classNamePrefix="react-select"
        defaultValue={[{ value: 'CelaPraha', label: 'Celá Praha' }]}
        onChange={(selectedItems: readonly any[]) => {
          onChange(selectedItems.map((x) => x.value));
        }}
      />
    </div>
  );
};
