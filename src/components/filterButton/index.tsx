import { Icon, Icons } from 'components/icon';
import React, { FC } from 'react';
import { Button } from 'react-bootstrap';
import './filterButton.scss';

export const FilterButton: FC<{
  textSmall?: boolean;
  isActive: boolean;
  action: () => void;
  variant: string;
  iconName: Icons;
  phaStcActive?: 'pha' | 'stc' | 'both';
  hintNotAvailable?: boolean;
}> = ({
  textSmall = false,
  isActive,
  action,
  variant,
  iconName,
  children,
  phaStcActive = 'pha',
  hintNotAvailable = false,
}) => {
  return (
    <Button
      active={isActive}
      variant={variant}
      className={`filter-button my-1 py-3 ${hintNotAvailable && `hintNotAvailable`}`}
      onClick={action}
      disabled={hintNotAvailable}
    >
      <div className="icon-wrapper">
        <Icon
          iconName={iconName}
          color={isActive ? 'primary' : 'secondary'}
          phaStcActive={phaStcActive}
          size={20}
        />
      </div>
      <div className={`label ${textSmall && `text-small`}`}>{children}</div>
    </Button>
  );
};
