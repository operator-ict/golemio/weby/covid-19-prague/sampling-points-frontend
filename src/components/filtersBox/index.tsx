import React, { FC, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { Filters } from 'types/filters';
import './filtersBox.scss';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {
  SamplingPointsServiceProcessTypeEnum,
  SamplingPointsServiceQueueTypeEnum,
  SamplingPointsServiceRequestTypeEnum,
} from 'api';
import { FilterButton } from 'components/filterButton';
import { DistrictSelect } from 'components/districtSelect';
import { getDerivedFilter } from 'utils/getDerivedFilter';
import { SamplingDatePicker } from 'components/samplingDatePicker';

export const FiltersBox: FC<{
  filters: Filters;
  changeFilters: (filters: Filters) => void;
}> = ({ filters, changeFilters }) => {
  const { t } = useTranslation();

  const changeFilter = useCallback(
    (name, value) => {
      const newFilters: Filters = { ...filters, [name]: value };

      changeFilters(getDerivedFilter(filters, newFilters));
    },
    [filters, changeFilters]
  );

  const changeFilterReservation = useCallback(() => {
    const newFilters: Filters = {
      ...filters,
      reservationType: SamplingPointsServiceQueueTypeEnum.Reservation,
      samplingType: SamplingPointsServiceProcessTypeEnum.Walk,
    };

    changeFilters(getDerivedFilter(filters, newFilters));
  }, [filters, changeFilters]);

  const changeFilterNoReservation = useCallback(() => {
    const newFilters: Filters = {
      ...filters,
      reservationType: SamplingPointsServiceQueueTypeEnum.Queue,
      samplingType: SamplingPointsServiceProcessTypeEnum.Walk,
    };

    changeFilters(getDerivedFilter(filters, newFilters));
  }, [filters, changeFilters]);

  const changeFilterDriveIn = useCallback(() => {
    const newFilters: Filters = {
      ...filters,
      reservationType: SamplingPointsServiceQueueTypeEnum.Reservation,
      samplingType: SamplingPointsServiceProcessTypeEnum.Drivein,
    };

    changeFilters(getDerivedFilter(filters, newFilters));
  }, [filters, changeFilters]);

  const changeFilterDates = (date: Date | undefined, hoursFrom: number, hoursTo: number) => {
    const newFilters: Filters = {
      ...filters,
      selectedDate: date,
      hoursFrom: hoursFrom,
      hoursTo: hoursTo,
    };

    changeFilters(getDerivedFilter(filters, newFilters));
  };

  return (
    <div className="filters-box">
      <div className="filters-box__filters-container">
        <h3>{t('filters.selectTestTypeHeader')}</h3>
        <Row className="filters-box__filter">
          <Col md={4} xl={3}>
            <FilterButton
              textSmall={true}
              iconName={'Request'}
              variant="outline-primary"
              isActive={filters.serviceType === SamplingPointsServiceRequestTypeEnum.Application}
              action={() => {
                changeFilter('serviceType', SamplingPointsServiceRequestTypeEnum.Application);
              }}
            >
              {t('filters.withRequest')}
            </FilterButton>
          </Col>
          <Col md={4} xl={3}>
            <FilterButton
              iconName={'Payment'}
              variant="outline-primary"
              action={() => {
                changeFilter('serviceType', SamplingPointsServiceRequestTypeEnum.Selfpaid);
              }}
              isActive={filters.serviceType === SamplingPointsServiceRequestTypeEnum.Selfpaid}
            >
              {t('filters.selfpaid')}
            </FilterButton>
          </Col>
          <Col md={4} xl={3}>
            <FilterButton
              iconName={'AgTests'}
              variant="outline-primary"
              action={() => {
                changeFilter('serviceType', SamplingPointsServiceRequestTypeEnum.Agtests);
              }}
              isActive={filters.serviceType === SamplingPointsServiceRequestTypeEnum.Agtests}
            >
              {t('filters.agtests')}
            </FilterButton>
          </Col>
        </Row>
      </div>
      <div className="filters-box__filters-container">
        <h3>{t('filters.samplingTypeHeader')}</h3>
        <Row className="filters-box__filter">
          <Col md={4} xl={3}>
            <FilterButton
              iconName={'WalkReservation'}
              variant="outline-primary"
              action={changeFilterReservation}
              isActive={
                filters.reservationType === SamplingPointsServiceQueueTypeEnum.Reservation &&
                filters.samplingType === SamplingPointsServiceProcessTypeEnum.Walk
              }
            >
              {t('filters.walkWithReservation')}
            </FilterButton>
          </Col>
          <Col md={4} xl={3}>
            <FilterButton
              iconName={'Walk'}
              variant="outline-primary"
              action={changeFilterNoReservation}
              isActive={
                filters.reservationType === SamplingPointsServiceQueueTypeEnum.Queue &&
                filters.samplingType === SamplingPointsServiceProcessTypeEnum.Walk
              }
              // hintNotAvailable={
              //   filters.serviceType === SamplingPointsServiceRequestTypeEnum.Agtests
              // }
            >
              {t('filters.walkWithoutReservation')}
            </FilterButton>
          </Col>
          <Col md={4} xl={3}>
            <FilterButton
              iconName={'DriveIn'}
              variant="outline-primary"
              action={changeFilterDriveIn}
              isActive={
                filters.reservationType === SamplingPointsServiceQueueTypeEnum.Reservation &&
                filters.samplingType === SamplingPointsServiceProcessTypeEnum.Drivein
              }
            >
              {t('filters.inCar')}
            </FilterButton>
          </Col>
        </Row>
      </div>
      <Row>
        <Col lg={6}>
          <div className="filters-box__filters-container">
            <h3>{t('filters.termHeader')}</h3>
            <div className="filters-box__filter">
              <SamplingDatePicker filters={filters} onChange={changeFilterDates} />
            </div>
          </div>
        </Col>
        <Col lg={6}>
          <div className="filters-box__filters-container">
            <h3>{t('filters.locationHeader')}</h3>
            <div className="filters-box__filter">
              <DistrictSelect
                filters={filters}
                onChange={(selectedItems: string[]) => {
                  changeFilter('districts', selectedItems);
                }}
              />
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};
