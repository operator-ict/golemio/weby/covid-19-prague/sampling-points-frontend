import { Link } from 'components/link';
import React, { FC } from 'react';

export const GoogleMapsLink: FC<{ lat: number; lng: number; children: string }> = ({
  lat,
  lng,
  children,
}) => {
  return (
    <Link
      href={`https://www.google.com/maps/search/?api=1&query=${lat},${lng}`}
      target="_blank"
      rel="noreferrer noopener"
    >
      {children}
    </Link>
  );
};
