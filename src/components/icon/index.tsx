import React, { FC } from 'react';
import { ReactComponent as Coins1 } from './icons/coins1.svg';
import { ReactComponent as Coins2 } from './icons/coins2.svg';
import { ReactComponent as Coins3 } from './icons/coins3.svg';
import { ReactComponent as DriveIn } from './icons/driveIn.svg';
import { ReactComponent as Payment } from './icons/payment.svg';
import { ReactComponent as Request } from './icons/request.svg';
import { ReactComponent as AgTests } from './icons/agtests.svg';
import { ReactComponent as Walk } from './icons/walk.svg';
import { ReactComponent as WalkReservation } from './icons/walkReservation.svg';
import { ReactComponent as AnyActive } from './icons/anyActive.svg';
import { ReactComponent as AnyNonActive } from './icons/anyNonActive.svg';
import { ReactComponent as PhaStc } from './icons/phaStc.svg';
import { ReactComponent as PhaActive } from './icons/phaActive.svg';
import { ReactComponent as PhaNonActive } from './icons/phaNonActive.svg';
import { ReactComponent as StcActive } from './icons/stcActive.svg';
import { ReactComponent as StcNonActive } from './icons/stcNonActive.svg';
import { ReactComponent as Marker } from './icons/marker.svg';
import { ReactComponent as Map } from './icons/map.svg';
import { ReactComponent as Calendar } from './icons/calendar.svg';
import { ReactComponent as Warning } from './icons/warning.svg';
import './icon.scss';
import classNames from 'classnames';

const iconList = {
  Coins1: Coins1,
  Coins2: Coins2,
  Coins3: Coins3,
  DriveIn: DriveIn,
  Payment: Payment,
  Request: Request,
  AgTests,
  Walk: Walk,
  WalkReservation: WalkReservation,
  AnyActive,
  AnyNonActive,
  PhaStc,
  PhaActive,
  PhaNonActive,
  StcActive,
  StcNonActive,
  Marker,
  Map,
  Calendar,
  Warning,
};

export type Icons = keyof typeof iconList;

export const Icon: FC<{
  iconName: Icons;
  size?: number;
  color?: 'primary' | 'secondary' | 'light';
  inverted?: boolean;
  phaStcActive?: 'pha' | 'stc' | 'both';
}> = ({
  iconName,
  size = 20,
  color = 'primary',
  inverted = false,
  phaStcActive = 'pha',
  ...props
}) => {
  const IconElement = iconList[iconName];
  return (
    <IconElement
      height={size}
      className={classNames('icon', color, { inverted }, phaStcActive)}
      {...props}
    />
  );
};
