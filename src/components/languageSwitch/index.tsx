import React, { FC, useContext } from 'react';
import { ButtonGroup, ToggleButton } from 'react-bootstrap';
import './languageSwitch.scss';
import { SamplingPointsLangEnum } from 'api';
import { LanguageContext } from 'state/languageState';

export const LanguageSwitch: FC = () => {
  const { changeLanguage, isLanguageSelected } = useContext(LanguageContext);

  return (
    <ButtonGroup toggle className="language-switch">
      <ToggleButton
        type="radio"
        name="languageSwitch"
        value="false"
        checked={isLanguageSelected(SamplingPointsLangEnum.Cs)}
        onChange={() => changeLanguage(SamplingPointsLangEnum.Cs)}
        className="language-switch__button btn-small"
      >
        Čeština
      </ToggleButton>
      <ToggleButton
        type="radio"
        name="languageSwitch"
        value="true"
        checked={isLanguageSelected(SamplingPointsLangEnum.En)}
        onChange={() => changeLanguage(SamplingPointsLangEnum.En)}
        className="language-switch__button btn-small"
      >
        English
      </ToggleButton>
    </ButtonGroup>
  );
};
