import { DataBox } from 'components/dataBox';
import { FiltersBox } from 'components/filtersBox';
import { LanguageSwitch } from 'components/languageSwitch';
import { useTranslation } from 'react-i18next';
import { useAnalytics } from 'hooks/analytics';
import React, { FC, useCallback, useContext, useEffect, useState } from 'react';
import {
  Filters,
  MethodFilter,
  DistrictFilter,
  PriceFilter,
  CertificateFilter,
} from 'types/filters';
import {
  SamplingPointsServiceProcessTypeEnum,
  SamplingPointsServiceQueueTypeEnum,
  SamplingPointsServiceRequestTypeEnum,
} from 'api';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import './mainView.scss';
import { Faq } from 'components/faq';
import { Link } from 'components/link';
import { OldDataWarning } from 'components/oldDataWarning';
import { DataProvider } from 'state/dataState';
import { LanguageContext } from 'state/languageState';
import { CookieConsent } from 'components/cookieConsent';

export const MainView: FC = () => {
  const { t } = useTranslation();
  const [analytics] = useAnalytics();
  const { language } = useContext(LanguageContext);

  const [filters, setFilters] = useState<Filters>({
    districts: ['CelaPraha'],
    reservationType: SamplingPointsServiceQueueTypeEnum.Reservation,
    samplingType: SamplingPointsServiceProcessTypeEnum.Walk,
    serviceType: SamplingPointsServiceRequestTypeEnum.Application,
    hoursFrom: 8,
    hoursTo: 20,

    // TODO Below filters are now obsolete
    districtFilter: DistrictFilter.PHA,
    serviceTypeFilter: SamplingPointsServiceRequestTypeEnum.Application,
    methodFilter: MethodFilter.WALK_WITH_RESERVATION,
    priceFilter: PriceFilter.ANY,
    certificateFilter: CertificateFilter.ANY,
  });

  useEffect(
    () => {
      analytics.changeFilter(filters, true);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const changeFilters = useCallback(
    (newFilters: Filters) => {
      analytics.changeFilter(newFilters, false);
      setFilters(newFilters);
    },
    [setFilters, analytics]
  );

  const [isFaqOpen, setFaqOpen] = useState(false);

  const closeFaq = useCallback(() => {
    setFaqOpen(false);
  }, []);

  const openFaq = useCallback(() => {
    setFaqOpen(true);
  }, []);

  return (
    <DataProvider filters={filters} lang={language}>
      <div className="main-view__container">
        <div className="main-view__header-bezpecnost">
          <Container className="py-4">
            <Row>
              <Col md={9} xs={12}>
                <h1>{t('title')}</h1>
                <span
                  className="subheading"
                  dangerouslySetInnerHTML={{ __html: t('subtitle') }}
                ></span>
              </Col>
              <Col
                md={3}
                className="main-view__link-portal text-md-right text-sm-left d-none d-md-block"
              >
                <Link href="https://golemio.cz/" target="_blank">
                  <img
                    src={`${process.env.PUBLIC_URL}/logo_golemio.svg`}
                    alt="Golemio - Pražská datová platforma"
                    className="main-view__header-golemio-logo"
                  />
                </Link>
              </Col>
            </Row>
          </Container>
        </div>
        <div className="main-view__header-message">
          <Container className="py-2">
            <Row>
              <Col className="text-center">
                <p>
                  <span>{t('message')}</span>
                  <Link href={t('vaccinationLink')} target="_blank">
                    ockovani.praha.eu
                  </Link>
                </p>
              </Col>
            </Row>
          </Container>
        </div>
        <div className="main-view__header">
          <Container className="py-4">
            <Row>
              <Col md={{ span: 9, order: 'first' }} xs={{ span: 12, order: 'last' }}></Col>
              <Col
                md={{ span: 3, order: 'last' }}
                xs={{ span: 12, order: 'first' }}
                className="my-2 text-md-right text-sm-left"
              >
                <LanguageSwitch />
              </Col>
            </Row>
            <FiltersBox filters={filters} changeFilters={changeFilters} />
          </Container>
        </div>
        <div className="main-view__section">
          <Container>
            <DataBox openFaq={openFaq} filters={filters} />
          </Container>
        </div>
        <div className="main-view__footer">
          <Container className="py-4">
            <Row>
              <Col
                md={{ span: 4, order: 'first' }}
                xs={{ span: 12, order: 'last' }}
                className="justify-content-md-start"
              >
                <Navbar.Text className="p-2">Operátor ICT, 2020</Navbar.Text>
              </Col>
              <Col md={8} xs={12}>
                <Nav className="justify-content-md-end text-uppercase">
                  <Nav.Link href="https://golemio.cz" target="_blank" className="p-2">
                    {t('footer.golemio')}
                  </Nav.Link>
                  <Nav.Link
                    href="https://gitlab.com/operator-ict/golemio/code/covid-19-prague/covid-19-prague-frontend"
                    target="_blank"
                    className="p-2"
                  >
                    {t('footer.sourcecode')}
                  </Nav.Link>
                  <Nav.Link href="https://covidapi.docs.apiary.io" target="_blank" className="p-2">
                    {t('footer.apidocumentation')}
                  </Nav.Link>
                  <Nav.Link href="#" onClick={() => setFaqOpen(true)} className="p-2">
                    {t('faq')}
                  </Nav.Link>
                  <CookieConsent />
                </Nav>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
      <Faq open={isFaqOpen} onHide={closeFaq}></Faq>
      <OldDataWarning />
    </DataProvider>
  );
};
