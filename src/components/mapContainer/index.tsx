import React, { FC, useState } from 'react';
import { HexagonFill } from 'react-bootstrap-icons';
import ReactMapGL, { Marker } from 'react-map-gl';
import { SamplingPointWithGeometry } from 'types/data';
import './mapContainer.scss';

export const MapContainer: FC<{
  data: SamplingPointWithGeometry[];
  initialPosition?: { latitude: number; longitude: number };
}> = ({ data, initialPosition }) => {
  const [viewport, setViewport] = useState({
    latitude: initialPosition?.latitude || 50.0755,
    longitude: initialPosition?.longitude || 14.4378,
    zoom: 15,
  });

  if (!window.config.MAPBOX_TOKEN) {
    return null;
  }

  return (
    <ReactMapGL
      height={200}
      width="100%"
      {...viewport}
      mapboxApiAccessToken={window.config.MAPBOX_TOKEN}
      onViewportChange={(newViewport: typeof viewport) =>
        setViewport({
          latitude: newViewport.latitude,
          longitude: newViewport.longitude,
          zoom: newViewport.zoom,
        })
      }
    >
      {data.map((samplingPoint) => (
        <Marker
          key={samplingPoint.id}
          latitude={samplingPoint.geometry.coordinates[1]}
          longitude={samplingPoint.geometry.coordinates[0]}
        >
          {/* placeholder for marker icon */}
          <HexagonFill size={10} className="text-primary" />
        </Marker>
      ))}
    </ReactMapGL>
  );
};
