import React, { FC } from 'react';
import { propEq } from 'ramda';
import { Marker } from 'react-map-gl';
import classNames from 'classnames';
import { FilterResultEnum } from 'hooks/allFilters';
import { DetailInfo, SamplingPointWithGeometry, Service } from 'types/data';
import { Icon } from 'components/icon';
import { MarkerTooltip } from 'components/markerTooltip';
import { TableColumns } from 'types/sorting';
import './mapMarker.scss';

const getSuitableServices = (service: Service[]) =>
  service.filter(propEq('filterResult', FilterResultEnum.ALL_PASSED));

export const MapMarker: FC<{
  point: SamplingPointWithGeometry;
  services: Service[];
  showDetail: (detailInfo: DetailInfo) => void;
  columns: TableColumns;
}> = ({ point, services, showDetail, columns }) => {
  const suitableServices = getSuitableServices(services);
  const hasSuitableServices = suitableServices.length > 0;
  const nonSuitableServicesCount = services.length - suitableServices.length;

  return (
    <Marker
      latitude={point.geometry.coordinates[1]}
      longitude={point.geometry.coordinates[0]}
      className={classNames('map-marker', {
        'map-marker--with-services': hasSuitableServices,
      })}
    >
      <MarkerTooltip
        point={point}
        suitableServices={suitableServices}
        nonSuitableServicesCount={nonSuitableServicesCount}
        showDetail={showDetail}
        columns={columns}
      >
        <Icon
          iconName="Marker"
          size={hasSuitableServices ? 20 : 15}
          inverted={!hasSuitableServices}
        />
      </MarkerTooltip>
    </Marker>
  );
};
