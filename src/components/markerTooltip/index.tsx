import { WaitTimeStateEnum } from 'utils/waitTime';
import React, { FC, ReactElement } from 'react';
import { Button, Col, OverlayTrigger, Popover, Row, Table } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import moment from 'moment-timezone';
import classNames from 'classnames';
import { useTimeFromNow } from 'hooks/timeFromNow';
import { DetailInfo, SamplingPointWithGeometry, Service } from 'types/data';
import './markerTooltip.scss';
import { TableColumns } from 'types/sorting';

export const MarkerTooltip: FC<{
  children: ReactElement;
  point: SamplingPointWithGeometry;
  suitableServices: Service[];
  nonSuitableServicesCount: number;
  showDetail: (detailInfo: DetailInfo) => void;
  columns: TableColumns;
}> = ({ children, point, suitableServices, nonSuitableServicesCount, showDetail, columns }) => {
  const { t } = useTranslation();
  const hasSuitableServices = suitableServices.length > 0;
  const { outdated } = useTimeFromNow(60);

  return (
    <OverlayTrigger
      trigger="click"
      rootClose
      overlay={
        <Popover
          id={point.id.toString()}
          className={classNames('marker-tooltip', {
            'marker-tooltip--non-suitable': !hasSuitableServices,
          })}
        >
          <Popover.Title className="marker-tooltip__header">
            <Row className="align-items-center">
              <Col>
                <h2 className="marker-tooltip__name">{point.name}</h2>
              </Col>
            </Row>
          </Popover.Title>
          <Popover.Content className="marker-tooltip__content">
            {hasSuitableServices && (
              <Table className="marker-tooltip__table">
                <thead>
                  <tr>
                    <th>{t('suitableServicesOfPoint')}</th>
                    {columns.showPriceColumn && <th>{t('databoxTable.price')}</th>}
                    {columns.showExpectedWaitTime && (
                      <th className="text-right">{t('databoxTable.expectedWaitTime')}</th>
                    )}
                    {columns.showClosestSlotColumn && (
                      <th className="text-right">{t('databoxTable.closestSlot')}</th>
                    )}
                  </tr>
                </thead>
                <tbody>
                  {suitableServices.map((service) => (
                    <tr
                      key={service.id}
                      onClick={() =>
                        showDetail({
                          pointId: point.id,
                          serviceId: service.id,
                        })
                      }
                      className="table-row"
                    >
                      <td className="title">{service.title}</td>
                      {columns.showPriceColumn && (
                        <td className="price">
                          {!!service.price &&
                            service.currency &&
                            `${service.price} ${t(service.currency)}`}
                        </td>
                      )}
                      {columns.showExpectedWaitTime && (
                        <td className="expected-wait-time text-right">
                          {service.waitTimeState === WaitTimeStateEnum.Closed && t('closed')}
                          {service.waitTimeState === WaitTimeStateEnum.Unknown && t('unknown')}
                          {service.waitTimeState === WaitTimeStateEnum.Short && t('shortQueueTime')}
                          {service.waitTimeState === WaitTimeStateEnum.Specified &&
                            t('waitTimeMin', {
                              min: service.queue?.waitMin,
                            })}
                        </td>
                      )}
                      {columns.showClosestSlotColumn && (
                        <td className="closest-slot text-right">
                          {service.rs?.slots?.[0] ? (
                            <span
                              className={
                                outdated(service.rs.slots[0].updatedAt)
                                  ? 'text-danger'
                                  : 'text-normal'
                              }
                            >
                              {t('from')}{' '}
                              {moment(service.rs.slots[0].startAt).format('D. M. (H:mm)')}
                            </span>
                          ) : (
                            t('unknown')
                          )}
                        </td>
                      )}
                    </tr>
                  ))}
                </tbody>
              </Table>
            )}
            {nonSuitableServicesCount > 0 && (
              <p className="marker-tooltip__non-suitable-services">
                {t('nonSuitableServicesCount', {
                  count: nonSuitableServicesCount,
                })}
              </p>
            )}

            <Row>
              <Col className="text-center">
                <Button
                  variant={hasSuitableServices ? 'primary' : 'secondary'}
                  size="sm"
                  className="marker-tooltip__more-button text-uppercase mt-2"
                  onClick={() =>
                    showDetail({
                      pointId: point.id,
                      serviceId: hasSuitableServices
                        ? suitableServices[0].id
                        : point.services?.[0].id || 0,
                    })
                  }
                >
                  {t('moreOnMap')}
                </Button>
              </Col>
            </Row>
          </Popover.Content>
        </Popover>
      }
    >
      {children}
    </OverlayTrigger>
  );
};
