import React, { FC, useContext, useEffect, useState } from 'react';
import { Button, Toast } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { DataContext } from 'state/dataState';
import './oldDataWarning.scss';

export const OldDataWarning: FC = () => {
  const [timer, setTimer] = useState<NodeJS.Timeout | null>(null);
  const [isOpen, setOpen] = useState(false);
  const { t } = useTranslation();
  const { refreshData, points } = useContext(DataContext);

  useEffect(
    () => {
      if (timer) {
        clearTimeout(timer);
      }

      setOpen(false);
      const newTimer = setTimeout(() => {
        setOpen(true);
      }, window.config.DATA_REFRESH_WARNING_DELAY * 1000);

      setTimer(newTimer);
    }, // eslint-disable-next-line react-hooks/exhaustive-deps
    [points]
  );

  return (
    <div className="old-data-warning__container">
      <Toast show={isOpen} onClose={() => setOpen(false)} className="old-data-warning__toast">
        <Toast.Header>
          <strong className="mr-auto">{t('updateData')}</strong>
        </Toast.Header>
        <Toast.Body>
          <p>{t('oldDataWarning')}</p>
          <div className="text-center">
            <Button
              onClick={() => {
                setOpen(false);
                refreshData();
              }}
              className="old-data-warning__button"
            >
              {t('refresh')}
            </Button>
          </div>
        </Toast.Body>
      </Toast>
    </div>
  );
};
