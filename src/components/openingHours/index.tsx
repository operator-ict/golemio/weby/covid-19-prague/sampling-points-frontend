import React, { FC, useMemo } from 'react';
import './openingHours.scss';
import moment from 'moment-timezone';
import { SamplingPointsServicesOpeningHours } from 'api';
import { useTranslation } from 'react-i18next';
import { weekdays } from 'utils/weekdays';

export const OpeningHours: FC<{
  hours: SamplingPointsServicesOpeningHours[];
  dayIndexProp?: number;
  skipDayPrefix?: boolean;
}> = ({ hours, dayIndexProp, skipDayPrefix = false }) => {
  const { t } = useTranslation();
  const dayIndex = dayIndexProp ?? moment().weekday();
  const shortDayString = t('shortDays', { returnObjects: true })[dayIndex];

  const todayHours = useMemo(() => {
    const todayString = weekdays[dayIndex];

    const formatTime = (time: string) => {
      // discard minutes if they are 00
      const timeWithPointBetweenHoursAndMinutes = time.replace(/:/, '.');
      const timeWithoutMinutes = timeWithPointBetweenHoursAndMinutes.replace(/.00$/, '');
      const timeWithoutLeadingZero = timeWithoutMinutes.replace(/^0/, '');
      return timeWithoutLeadingZero;
    };

    const hoursInDay = hours.filter((hoursPart) => {
      return hoursPart.weekday === todayString;
    });

    if (!hoursInDay.length) {
      return t('closed');
    }

    return hoursInDay
      .map((hoursPart) => {
        return `${formatTime(hoursPart.startAt)}–${formatTime(hoursPart.endAt)}`;
      })
      .join(', ');
  }, [hours, dayIndex, t]);

  return (
    <>
      {!!hours.length && (
        <>
          {!skipDayPrefix && <>{shortDayString} </>}
          {todayHours}
        </>
      )}
    </>
  );
};
