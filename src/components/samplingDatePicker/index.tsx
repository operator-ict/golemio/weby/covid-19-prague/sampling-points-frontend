import classNames from 'classnames';
import { Icon } from 'components/icon';
import React, { FC, useState } from 'react';
import DatePicker from 'components/datePicker';
import { Filters } from 'types/filters';
import { useTranslation } from 'react-i18next';

export const SamplingDatePicker: FC<{
  filters: Filters;
  onChange: (date: Date | undefined, hoursFrom: number, hoursTo: number) => void;
}> = ({ filters, onChange }) => {
  const { t } = useTranslation();

  const [temporaryDateWithTimes, setTemporaryDateWithTimes] = useState({
    date: new Date(),
    hoursFrom: 8,
    hoursTo: 20,
  });

  const changeDate = (date: Date | null, hoursFrom: number, hoursTo: number) => {
    if (date) {
      setTemporaryDateWithTimes({
        date: date,
        hoursFrom: hoursFrom,
        hoursTo: hoursTo,
      });
    }
  };

  const close = () => {
    onChange(
      temporaryDateWithTimes.date,
      temporaryDateWithTimes.hoursFrom,
      temporaryDateWithTimes.hoursTo
    );
  };

  const cancel = () => {
    setTemporaryDateWithTimes({ ...temporaryDateWithTimes, date: new Date() });
    onChange(undefined, temporaryDateWithTimes.hoursFrom, temporaryDateWithTimes.hoursTo);
  };

  const isActive = filters.selectedDate !== undefined;

  return (
    <div
      className={classNames('district-select', 'filter-button', {
        'active btn-outline-primary': isActive,
      })}
    >
      <Icon iconName="Calendar" color={isActive ? 'primary' : 'secondary'} />
      <DatePicker
        onChange={changeDate}
        onClose={close}
        onClickOutside={close}
        onCancel={cancel}
        value={temporaryDateWithTimes.date}
        isDateEmpty={filters.selectedDate === undefined}
        times={temporaryDateWithTimes}
        label={''}
        maxDate={new Date(Date.now() + 12096e5)}
        translate={t}
        filterDate={(date) => {
          return new Date(new Date().setHours(0, 0, 0, 0)) <= date;
        }}
      />
    </div>
  );
};
