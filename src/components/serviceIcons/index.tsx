import React, { FC } from 'react';
import { Icon } from 'components/icon';
import {
  SamplingPointsService,
  SamplingPointsServiceProcessTypeEnum,
  SamplingPointsServiceQueueTypeEnum,
  SamplingPointsServiceRequestTypeEnum,
} from 'api';
import { prices, usePriceFilter } from 'hooks/priceFilter';
import { useMethodFilter } from 'hooks/methodFilter';
import { usePaymentFilter } from 'hooks/paymentFilter';
import { CertificateFilter, Filters, PriceFilter } from 'types/filters';
import './serviceIcons.scss';
import { useTranslation } from 'react-i18next';
import { useDistrictFilter } from 'hooks/districtFilter';
import { FilterResultEnum, useFilters } from 'hooks/allFilters';
import { CheckCircleFill, XCircleFill } from 'react-bootstrap-icons';
import classNames from 'classnames';
import { SimpleTooltip } from 'components/simpleTooltip';

const ICON_SIZE = 20;

const HighlightText: FC<{ highlight: boolean }> = ({ highlight, children }) => {
  return (
    <span
      className={classNames('service-icons__text', {
        'service-icons__text--highlight': highlight,
      })}
    >
      {children}
    </span>
  );
};

const getDistrictActiveValue = (district?: string | null) => {
  switch (district) {
    case 'PHA':
      return 'pha';
    case 'STC':
      return 'stc';
    default:
      return 'both';
  }
};

export const ServiceIcons: FC<{
  filters: Filters;
  service: SamplingPointsService;
  district?: string | null;
  districtName?: string | null;
  colors?: 'inverted' | 'normal';
  isInDetail?: boolean;
}> = ({ filters, service, colors = 'normal', district, districtName, isInDetail = false }) => {
  const { t } = useTranslation();

  const { hasAppropriatePrice } = usePriceFilter();
  const { hasAppropriateMethod } = useMethodFilter();
  const { hasAppropriatePayment } = usePaymentFilter();
  const { hasAppropriateDistrict } = useDistrictFilter();
  const { getFilterResult } = useFilters();

  const paymentFilterResult = hasAppropriatePayment(service, filters);
  const methodFilterResult = hasAppropriateMethod(service, filters);
  const priceFilterResult = hasAppropriatePrice(service, filters);
  const districtFilterResult = district && hasAppropriateDistrict(district, filters);

  const filterResult = getFilterResult(service);

  const districtTooltipText = districtName;
  const phaStcActiveValue = getDistrictActiveValue(district);

  return (
    <div className={isInDetail ? 'text-right' : undefined}>
      <SimpleTooltip
        id=""
        className="display-absolute"
        text={
          <div className="service-icons">
            {district && (
              <div className="service-icons__item">
                <div className="service-icons__icon-holder">
                  <Icon
                    inverted={colors === 'inverted'}
                    iconName="PhaStc"
                    size={ICON_SIZE}
                    color={districtFilterResult ? 'light' : 'secondary'}
                    phaStcActive={phaStcActiveValue}
                  />
                </div>
                <HighlightText highlight={districtFilterResult || true}>
                  {districtTooltipText}
                </HighlightText>
              </div>
            )}
            {service.requestType === SamplingPointsServiceRequestTypeEnum.Selfpaid && (
              <div className="service-icons__item">
                <div className="service-icons__icon-holder">
                  <Icon
                    inverted={colors === 'inverted'}
                    iconName="Payment"
                    size={ICON_SIZE}
                    color={paymentFilterResult ? 'light' : 'secondary'}
                  />
                </div>
                <HighlightText highlight={paymentFilterResult}>
                  {t('filters.selfpaid')}
                </HighlightText>
              </div>
            )}
            {service.requestType === SamplingPointsServiceRequestTypeEnum.Application && (
              <div className="service-icons__item">
                <div className="service-icons__icon-holder">
                  <Icon
                    inverted={colors === 'inverted'}
                    iconName="Request"
                    size={ICON_SIZE}
                    color={paymentFilterResult ? 'light' : 'secondary'}
                  />
                </div>
                <HighlightText highlight={paymentFilterResult}>
                  {t('filters.withRequest')}
                </HighlightText>
              </div>
            )}
            {service.requestType === SamplingPointsServiceRequestTypeEnum.Agtests && (
              <div className="service-icons__item">
                <div className="service-icons__icon-holder">
                  <Icon
                    inverted={colors === 'inverted'}
                    iconName="AgTests"
                    size={ICON_SIZE}
                    color={paymentFilterResult ? 'light' : 'secondary'}
                  />
                </div>
                <HighlightText highlight={paymentFilterResult}>
                  {t('filters.agtests')}
                </HighlightText>
              </div>
            )}
            {service.processType === SamplingPointsServiceProcessTypeEnum.Drivein && (
              <div className="service-icons__item">
                <div className="service-icons__icon-holder">
                  <Icon
                    inverted={colors === 'inverted'}
                    iconName="DriveIn"
                    size={ICON_SIZE}
                    color={methodFilterResult ? 'light' : 'secondary'}
                  />
                </div>
                <HighlightText highlight={methodFilterResult}>{t('filters.inCar')}</HighlightText>
              </div>
            )}
            {service.processType === SamplingPointsServiceProcessTypeEnum.Walk &&
              service.queueType === SamplingPointsServiceQueueTypeEnum.Queue && (
                <div className="service-icons__item">
                  <div className="service-icons__icon-holder">
                    <Icon
                      inverted={colors === 'inverted'}
                      iconName="Walk"
                      size={ICON_SIZE}
                      color={methodFilterResult ? 'light' : 'secondary'}
                    />
                  </div>
                  <HighlightText highlight={methodFilterResult}>
                    {t('filters.walkWithoutReservation')}
                  </HighlightText>
                </div>
              )}
            {service.processType === SamplingPointsServiceProcessTypeEnum.Walk &&
              service.queueType === SamplingPointsServiceQueueTypeEnum.Reservation && (
                <div className="service-icons__item">
                  <div className="service-icons__icon-holder">
                    <Icon
                      inverted={colors === 'inverted'}
                      iconName="WalkReservation"
                      size={ICON_SIZE}
                      color={methodFilterResult ? 'light' : 'secondary'}
                    />
                  </div>
                  <HighlightText highlight={methodFilterResult}>
                    {t('filters.walkWithReservation')}
                  </HighlightText>
                </div>
              )}
            {((filters.serviceTypeFilter === SamplingPointsServiceRequestTypeEnum.Selfpaid &&
              filters.priceFilter === PriceFilter.LOW) ||
              filters.certificateFilter === CertificateFilter.YES) && (
              <>
                <hr color="#fff" />
                <div className="service-icons__additional-filters">
                  {filters.serviceTypeFilter === SamplingPointsServiceRequestTypeEnum.Selfpaid &&
                    filters.priceFilter === PriceFilter.LOW && (
                      <div>
                        <HighlightText highlight={priceFilterResult}>
                          {t('filters.priceOrLess', { price: prices.low })}
                        </HighlightText>
                      </div>
                    )}
                  {filters.certificateFilter === CertificateFilter.YES && (
                    <div>
                      <HighlightText highlight={service.certificate}>
                        {t('withCertificate')}
                      </HighlightText>
                    </div>
                  )}
                </div>
              </>
            )}
          </div>
        }
      >
        {filterResult === FilterResultEnum.ALL_PASSED && (
          <div className="checkbox-wrapper green">
            <CheckCircleFill
              className={colors === 'normal' ? 'text-primary-green' : 'text-white'}
              size={30}
            />
          </div>
        )}
        {filterResult !== FilterResultEnum.ALL_PASSED && (
          <div className="checkbox-wrapper">
            <XCircleFill
              className={colors === 'normal' ? 'text-secondary' : 'text-white'}
              size={30}
            />
          </div>
        )}
      </SimpleTooltip>
    </div>
  );
};
