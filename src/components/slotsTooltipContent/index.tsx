import { SamplingPointsServicesRsSlots } from 'api';
import moment from 'moment';
import * as React from 'react';
import { useTranslation } from 'react-i18next';
import './slotsTooltipContent.scss';

interface Props {
  updatedWhen: string;
  slots: SamplingPointsServicesRsSlots[];
  outdated: boolean;
}

export const SlotsTooltipContent: React.FC<Props> = ({ updatedWhen, slots, outdated }) => {
  const { t } = useTranslation();
  return (
    <div className="slots-tooltip-content">
      <span className="slots-tooltip-content__header">{t('closestSlot')}</span>
      <table className="slots-tooltip-content__table">
        <tbody>
          {slots.map((slot) => (
            <tr key={slot.id}>
              <td>{moment(slot.startAt).format('D. M.')}</td>
              <td>{moment(slot.startAt).format('H.mm')}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <span className={outdated ? 'text-danger' : ''}>{updatedWhen}</span>
    </div>
  );
};
