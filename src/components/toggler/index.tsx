import React from 'react';
import { Form } from 'react-bootstrap';
import './toggler.scss';

interface Props {
  checked: boolean;
  label: string;
  id?: string;
  onChange?: () => void;
}

export const Toggler: React.FC<Props> = (props) => (
  <Form.Check className="toggler" type="switch" {...props} />
);
