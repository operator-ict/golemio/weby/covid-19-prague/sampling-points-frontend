export {};

declare global {
  interface Window {
    plausible: (...args: any[]) => void;
    config: {
      API_URL: string;
      MAPBOX_TOKEN: string;
      EMAIL: string;
      GA_CODE: string;
      GA_CODE_COOKIELESS: string;
      DATA_REFRESH_WARNING_DELAY: number;
    };
  }
}
