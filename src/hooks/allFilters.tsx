import { SamplingPointsService } from 'api';

export enum FilterResultEnum {
  ALL_PASSED,
  MAIN_PASSED,
  FAILED,
}

export const useFilters = () => {
  return {
    getFilterResult: (service: SamplingPointsService) => {
      return service.filterMatched ? FilterResultEnum.ALL_PASSED : FilterResultEnum.FAILED;
    },
  };
};
