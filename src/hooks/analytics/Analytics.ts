import { SortColumn } from 'types/sorting';
import ga from 'react-ga';
import {
  Filters,
  DistrictFilter,
  MethodFilter,
  PriceFilter,
  CertificateFilter,
} from 'types/filters';

interface ServiceParams {
  serviceId: number;
  pointId: number;
}

const isDev = process.env.NODE_ENV === 'development';

/**
 * Singleton Analytics with implementation of React Google Analytics
 */
export class Analytics {
  private static instance: Analytics;

  private constructor() {
    // ga.initialize(window.config.GA_CODE, { debug: isDev });
    ga.initialize(
      [
        {
          trackingId: window.config.GA_CODE,
          gaOptions: {
            name: 'tracker1',
            userId: 'accept',
          },
        },
        {
          trackingId: window.config.GA_CODE_COOKIELESS,
          gaOptions: { name: 'tracker2', storage: 'none' },
        },
      ],
      { alwaysSendToDefaultTracker: false }
    );
    ga.set({ anonymizeIp: true }, ['tracker2']);
  }

  public static getInstance(): Analytics {
    if (!Analytics.instance) {
      Analytics.instance = new Analytics();
    }

    return Analytics.instance;
  }

  public openPage(url: string) {
    ga.pageview(url, ['tracker1', 'tracker2']);
  }

  public openModal({ serviceId, pointId }: ServiceParams) {
    ga.modalview(`/${serviceId}/${pointId}`, ['tracker1', 'tracker2']);
    ga.event(
      {
        category: 'User',
        action: 'Open Modal',
        label: pointId.toString(),
        value: serviceId,
      },
      ['tracker1', 'tracker2']
    );
  }

  public openAbout(type: string) {
    ga.event(
      {
        category: 'User',
        action: 'Open About Modal',
        label: type,
      },
      ['tracker1', 'tracker2']
    );
  }

  public changeFilter(filters: Filters, isDefault: boolean = false) {
    let label = '';
    label += isDefault ? '[default] ' : '';
    label += DistrictFilter[filters.districtFilter] + ',';
    label += MethodFilter[filters.districtFilter] + ',';
    label += CertificateFilter[filters.certificateFilter] + ',';
    label += 'PRICE_' + PriceFilter[filters.priceFilter];
    ga.event(
      {
        category: 'User',
        action: 'Change filter',
        label: label,
      },
      ['tracker1', 'tracker2']
    );
  }

  public changeDataView(shouldShowMap: boolean, isDefault: boolean = false) {
    ga.event(
      {
        category: 'User',
        action: 'Change data view',
        label: (isDefault ? '[default] ' : '') + (shouldShowMap ? 'map' : 'table'),
      },
      ['tracker1', 'tracker2']
    );
  }

  public changeSort(sortBy: SortColumn, isDefault: boolean = false) {
    ga.event(
      {
        category: 'User',
        action: 'Change sorting',
        label: (isDefault ? '[default] ' : '') + sortBy,
      },
      ['tracker1', 'tracker2']
    );
  }

  public changeService({ serviceId, pointId }: ServiceParams, isDefault: boolean = false) {
    ga.event(
      {
        category: 'User',
        action: 'Change Service',
        label: (isDefault ? '[default] ' : '') + pointId.toString(),
        value: serviceId,
      },
      ['tracker1', 'tracker2']
    );
  }

  public book({ serviceId, pointId, url }: ServiceParams & { url?: string | null }) {
    if (!url) {
      return;
    }

    ga.outboundLink(
      { label: url },
      () => {
        ga.event({
          category: 'External Navigation',
          action: 'Click on the Link to book a service',
          label: pointId.toString(),
          value: serviceId,
        });
      },
      ['tracker1', 'tracker2']
    );
  }

  public openExternalLink(url: string) {
    ga.outboundLink({ label: url }, () => {}, ['tracker1', 'tracker2']);
  }
}
