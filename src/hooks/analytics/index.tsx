import * as React from 'react';
import { Analytics } from './Analytics';

export const useAnalytics = () => {
  const [analytics] = React.useState(Analytics.getInstance());
  return [analytics];
};
