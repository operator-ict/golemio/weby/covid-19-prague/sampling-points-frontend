import { SamplingPointsService } from 'api';
import { CertificateFilter, Filters } from 'types/filters';

export const useCertificateFilter = () => {
  return {
    hasAppropriateCertificate: (service: SamplingPointsService, filters: Filters): boolean => {
      return (
        filters.certificateFilter === CertificateFilter.ANY ||
        (filters.certificateFilter === CertificateFilter.YES && service.certificate) ||
        (filters.certificateFilter === CertificateFilter.NO && !service.certificate)
      );
    },
  };
};
