import { Filters } from 'types/filters';

export const useDistrictFilter = () => {
  return {
    hasAppropriateDistrict: (
      district: string | undefined,
      filters: Pick<Filters, 'districtFilter'> & Partial<Filters>
    ): boolean => {
      // TODO: Consider removing?
      return true;
    },
  };
};
