import {
  SamplingPointsService,
  SamplingPointsServiceProcessTypeEnum,
  SamplingPointsServiceQueueTypeEnum,
} from 'api';
import { Filters, MethodFilter } from 'types/filters';

export const useMethodFilter = () => {
  return {
    hasAppropriateMethod: (service: SamplingPointsService, filters: Filters): boolean => {
      const method = service.processType;
      const reservation = service.queueType;

      switch (filters.methodFilter) {
        case MethodFilter.IN_CAR:
          return (
            method === SamplingPointsServiceProcessTypeEnum.Drivein &&
            reservation === SamplingPointsServiceQueueTypeEnum.Reservation
          );
        case MethodFilter.WALK_WITHOUT_RESERVATION:
          return (
            method === SamplingPointsServiceProcessTypeEnum.Walk &&
            reservation === SamplingPointsServiceQueueTypeEnum.Queue
          );
        case MethodFilter.WALK_WITH_RESERVATION:
          return (
            method === SamplingPointsServiceProcessTypeEnum.Walk &&
            reservation === SamplingPointsServiceQueueTypeEnum.Reservation
          );
      }
    },
  };
};
