import { SamplingPointsService, SamplingPointsServiceRequestTypeEnum } from 'api';
import { Filters, PriceFilter } from 'types/filters';

export const prices = {
  low: 810,
  high: 2500,
};

export const usePriceFilter = () => {
  return {
    hasAppropriatePrice: (
      service: SamplingPointsService,
      filters: Pick<Filters, 'priceFilter'> & Partial<Filters>
    ): boolean => {
      if (filters.serviceTypeFilter === SamplingPointsServiceRequestTypeEnum.Application) {
        return true;
      }

      const price = service.price;

      switch (filters.priceFilter) {
        case PriceFilter.LOW:
          return price === undefined || price <= prices.low;
        case PriceFilter.HIGH:
          return price === undefined || price <= prices.high;
        case PriceFilter.ANY:
          return true;
      }
    },
  };
};
