import { WaitTimeStateEnum } from 'utils/waitTime';
import { WeightedService } from 'types/data';

const MAX_DATE = new Date().setFullYear(3000);

const getWaitTimeStateScore = (state: WaitTimeStateEnum) => {
  switch (state) {
    case WaitTimeStateEnum.Short:
      return 0;
    case WaitTimeStateEnum.Specified:
      return 0;
    case WaitTimeStateEnum.Unknown:
      return 1;
    case WaitTimeStateEnum.Closed:
      return 2;
  }
};

export const useSorters = () => {
  return {
    sortByResultTimeHours: (a: WeightedService, b: WeightedService) => {
      const resultTimeHoursA = a.resultTimeHours || Infinity;
      const resultTimeHoursB = b.resultTimeHours || Infinity;
      return resultTimeHoursA - resultTimeHoursB;
    },
    sortByPrice: (a: WeightedService, b: WeightedService) => {
      return a.price - b.price;
    },
    sortByWaitTime: (a: WeightedService, b: WeightedService) => {
      const stateA = a.waitTimeState;
      const stateB = b.waitTimeState;

      const minA = stateA === WaitTimeStateEnum.Short ? 30 : a.queue?.waitMin;
      const minB = stateA === WaitTimeStateEnum.Short ? 30 : b.queue?.waitMin;

      const scoreA = getWaitTimeStateScore(stateA);
      const scoreB = getWaitTimeStateScore(stateB);

      if (scoreA === scoreB) {
        if (scoreA !== 0) {
          return 0;
        }

        if (!minA) {
          return 1;
        } else if (!minB) {
          return -1;
        } else {
          return minA - minB;
        }
      }

      return scoreA - scoreB;
    },
    sortByClosestSlot: (a: WeightedService, b: WeightedService) => {
      const slotA = a.rs?.slots?.[0]?.startAt || MAX_DATE;
      const slotB = b.rs?.slots?.[0]?.startAt || MAX_DATE;

      return +slotA - +slotB;
    },
  };
};
