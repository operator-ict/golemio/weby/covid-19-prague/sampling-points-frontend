import {
  SamplingPointsServiceRequestTypeEnum,
  SamplingPointsServiceProcessTypeEnum,
  SamplingPointsServiceQueueTypeEnum,
} from 'api';
import { Filters, PriceFilter } from 'types/filters';
import { SortColumn, TableColumns } from 'types/sorting';
import { prices } from 'hooks/priceFilter';
import { useTranslation } from 'react-i18next';
import { useMemo, useContext } from 'react';
import { DataContext } from 'state/dataState';

export const useSubHeading = (
  filters: Filters,
  columns: TableColumns,
  sortByColumn?: SortColumn
) => {
  const { t } = useTranslation();
  const { districts } = useContext(DataContext);

  const subHeading = useMemo(() => {
    const getCsDistrictName = (districtName: string) => {
      const targetDistrict = districts.find((el) => {
        return el.districtName === districtName;
      });
      return targetDistrict?.districtNameCs || 'Celá Praha';
    };
    let districtString = '';
    filters.districts.map(
      (el, index) => (districtString += (!index ? '' : ', ') + getCsDistrictName(el))
    );

    const paymentString = (
      (filters.serviceType === SamplingPointsServiceRequestTypeEnum.Application &&
        t('filters.withRequest').toLowerCase()) ||
      (filters.serviceType === SamplingPointsServiceRequestTypeEnum.Selfpaid &&
        t('filters.selfpaid').toLowerCase()) ||
      t('filters.agtests').toLowerCase()
    ).replace(/pcr/g, 'PCR');

    const methodString =
      (filters.samplingType === SamplingPointsServiceProcessTypeEnum.Walk &&
        filters.reservationType === SamplingPointsServiceQueueTypeEnum.Reservation &&
        t('filters.walkWithReservation').toLowerCase()) ||
      (filters.samplingType === SamplingPointsServiceProcessTypeEnum.Walk &&
        filters.reservationType === SamplingPointsServiceQueueTypeEnum.Queue &&
        t('filters.walkWithoutReservation').toLowerCase()) ||
      (filters.samplingType === SamplingPointsServiceProcessTypeEnum.Drivein &&
        t('filters.inCar').toLowerCase());

    const getPriceFilterName = (priceFilter: PriceFilter) => {
      switch (priceFilter) {
        case PriceFilter.ANY:
          return t('filters.any').toLowerCase();
        case PriceFilter.HIGH:
          return t('filters.priceOrLess', {
            price: prices.high,
          }).toLowerCase();
        case PriceFilter.LOW:
          return t('filters.priceOrLess', { price: prices.low }).toLowerCase();
      }
    };

    const priceString = columns.showPriceColumn
      ? `${t('filters.price').toLowerCase()} ${getPriceFilterName(filters.priceFilter)}`
      : null;

    const filterString =
      (sortByColumn === SortColumn.PRICE && t('databoxSubheadingOrder.byPrice')) ||
      (sortByColumn === SortColumn.WAIT_TIME_OR_CLOSEST_SLOT &&
        columns.showClosestSlotColumn &&
        t('databoxSubheadingOrder.byTime')) ||
      (sortByColumn === SortColumn.WAIT_TIME_OR_CLOSEST_SLOT &&
        columns.showExpectedWaitTime &&
        t('databoxSubheadingOrder.byWaitTime')) ||
      t('databoxSubheadingOrder.byResultTime');

    let resultArr = [
      paymentString,
      methodString,
      ...(priceString ? [priceString] : []),
      ...(sortByColumn ? [filterString] : []),
    ];

    if (districtString) resultArr.unshift(districtString);

    return [...resultArr].join(', ');
  }, [filters, columns, sortByColumn, t]);

  return { subHeading };
};
