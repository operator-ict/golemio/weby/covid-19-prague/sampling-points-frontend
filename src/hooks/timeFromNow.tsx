import { useEffect, useState } from 'react';
import moment from 'moment-timezone';

export const useTimeFromNow = (refreshIntervalSeconds: number) => {
  const [time, setTime] = useState(Date.now());

  useEffect(() => {
    const interval = setInterval(() => setTime(Date.now()), refreshIntervalSeconds * 1000);
    return () => clearInterval(interval);
  }, [refreshIntervalSeconds]);

  return {
    getTimeFromNow: (timeStamp: Date) => moment(timeStamp).from(moment(time)),
    outdated: (timeStamp: Date) =>
      Math.abs(moment(timeStamp).diff(moment(time), 'seconds')) > 60 * 30,
  };
};
