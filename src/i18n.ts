import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { resources } from 'locales';

const isDev = process.env.NODE_ENV === 'development';

i18n.use(initReactI18next).init({
  resources,
  lng: 'cs',
  fallbackLng: 'cs',
  debug: isDev,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
