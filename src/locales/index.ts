import translationCs from './translation/cs.json';
import translationEn from './translation/en.json';

export const resources = {
  // cs: language key
  // translation: namespace
  // translationCs: resource file
  cs: { translation: translationCs },
  en: { translation: translationEn },
};
