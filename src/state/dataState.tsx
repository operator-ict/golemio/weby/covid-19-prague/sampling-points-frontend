import React, { createContext, FC, useCallback, useEffect, useMemo, useState } from 'react';
import {
  Configuration,
  District,
  DistrictsApi,
  SamplingPointsApi,
  SamplingPointsLangEnum,
  SamplingPointsOutputFeatures,
  SamplingPointsRequest,
} from 'api';
import { SamplingPointWithGeometry, Service, WeightedService } from 'types/data';
import { getWaitTimeState } from 'utils/waitTime';
import { Filters } from 'types/filters';
import { useFilters } from 'hooks/allFilters';

const samplingPointsApi = new SamplingPointsApi(
  new Configuration({ basePath: window.config.API_URL })
);
const fetchSamplingPointsData = async (request: SamplingPointsRequest) =>
  samplingPointsApi.samplingPoints(request);

const districtsApi = new DistrictsApi(new Configuration({ basePath: window.config.API_URL }));
const fetchDistrictsData = async () => districtsApi.districts();

const getSamplingPoints = (
  features: SamplingPointsOutputFeatures[] | undefined
): SamplingPointWithGeometry[] => {
  if (!features) {
    return [];
  }
  return features.map((samplingPoint) => ({
    ...samplingPoint.properties,
    geometry: samplingPoint.geometry,
  }));
};

const getServices = (samplingPoints: SamplingPointWithGeometry[] | undefined): Service[] => {
  if (!samplingPoints) {
    return [];
  }
  return samplingPoints.reduce((acc: Service[], { services }) => {
    if (!services) return acc;

    const pointServices = services.map((service) => ({
      ...service,
      waitTimeState: getWaitTimeState({
        queue: service.queue,
        openingHours: service.openingHours,
      }),
    }));

    return [...acc, ...pointServices];
  }, []);
};

export type DataState = {
  points: SamplingPointWithGeometry[];
  districts: District[];
  services: WeightedService[];
  refreshData: () => void;
  isLoading: boolean;
  hasError: boolean;
};

const defaultState = {
  points: [],
  districts: [],
  services: [],
  refreshData: () => {},
  isLoading: false,
  hasError: false,
};

export const DataContext = createContext<DataState>(defaultState);

export const DataProvider: FC<{ filters: Filters; lang: SamplingPointsLangEnum }> = ({
  children,
  filters,
  lang,
}) => {
  const [isLoading, setIsLoading] = useState(defaultState.isLoading);
  const [hasError, setHasError] = useState(defaultState.hasError);
  const [points, setPoints] = useState<SamplingPointWithGeometry[]>(defaultState.points);
  const [districts, setDistricts] = useState<District[]>(defaultState.districts);

  const { getFilterResult } = useFilters();

  let dateFrom = filters.selectedDate;
  let dateTo = filters.selectedDate;

  if (filters.selectedDate) {
    dateFrom = new Date(dateFrom!);
    dateTo = new Date(dateTo!);

    dateFrom.setHours(filters.hoursFrom, 0, 0, 0);
    dateTo.setHours(filters.hoursTo, 0, 0, 0);
  }

  const districtFilterValues = filters.districts.length ? filters.districts : undefined;

  const request = {
    lang: lang,
    district: districtFilterValues,
    serviceRequestType: filters.serviceType,
    serviceQueueType: filters.reservationType,
    serviceProcessType: filters.samplingType,
    serviceSlotsFreeFrom: dateFrom,
    serviceSlotsFreeTo: dateTo,
  };

  const refreshSamplingPointsData = useCallback(async () => {
    try {
      setIsLoading(true);
      const data = await fetchSamplingPointsData(request);
      const newPoints = getSamplingPoints(data?.features);
      setPoints(newPoints);
      setHasError(false);
    } catch (error) {
      console.error(error);
      setHasError(true);
    } finally {
      setIsLoading(false);
    }
  }, [lang, filters]);

  const refreshDistrictsData = useCallback(async () => {
    try {
      const districts = await fetchDistrictsData();
      setDistricts(districts);
      setHasError(false);
    } catch (error) {
      console.error(error);
      setHasError(true);
    }
  }, []);

  useEffect(() => {
    refreshSamplingPointsData();
  }, [refreshSamplingPointsData, lang, filters]);

  useEffect(() => {
    refreshDistrictsData();
  }, []);

  const services = useMemo(() => getServices(points), [points]);

  const weightedServices = useMemo(() => {
    if (!services) {
      return services;
    }

    return services.map((service) => {
      return {
        ...service,
        filterResult: getFilterResult(service),
      };
    });
  }, [filters, getFilterResult, points, services]);

  const state = useMemo((): DataState => {
    return {
      isLoading,
      hasError,
      points,
      services: weightedServices,
      districts,
      refreshData: refreshSamplingPointsData,
    };
  }, [isLoading, hasError, points, weightedServices, districts, refreshSamplingPointsData]);

  return <DataContext.Provider value={state}>{children}</DataContext.Provider>;
};
