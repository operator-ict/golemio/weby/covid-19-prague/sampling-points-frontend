import React, { createContext, FC, useCallback, useState } from 'react';
import { SamplingPointsLangEnum } from 'api';
import i18n from 'i18n';
import moment from 'moment-timezone';
import 'moment/locale/cs';

const getLanguageFromString = (langString: string | null) => {
  switch (langString as SamplingPointsLangEnum) {
    case SamplingPointsLangEnum.Cs:
      return SamplingPointsLangEnum.Cs;
    case SamplingPointsLangEnum.En:
      return SamplingPointsLangEnum.En;
    default:
      return null;
  }
};

const languageStorageKey = 'lang';
const savedLanguage = getLanguageFromString(localStorage.getItem(languageStorageKey));

const defaultLanguage = savedLanguage || SamplingPointsLangEnum.Cs;

i18n.changeLanguage(defaultLanguage);
moment.locale(defaultLanguage);

export const LanguageContext = createContext<{
  language: SamplingPointsLangEnum;
  changeLanguage: (newLanguage: SamplingPointsLangEnum) => void;
  isLanguageSelected: (language: SamplingPointsLangEnum) => boolean;
}>({ language: defaultLanguage, changeLanguage: () => {}, isLanguageSelected: () => false });

export const LanguageProvider: FC = ({ children }) => {
  const [language, setLanguage] = useState(defaultLanguage);

  const changeLanguage = useCallback((newLanguage: SamplingPointsLangEnum) => {
    setLanguage(newLanguage);
    i18n.changeLanguage(newLanguage);
    moment.locale(newLanguage);
    localStorage.setItem(languageStorageKey, newLanguage);
  }, []);

  const isLanguageSelected = useCallback(
    (lang: SamplingPointsLangEnum) => {
      return language === lang;
    },
    [language]
  );

  return (
    <LanguageContext.Provider value={{ language, changeLanguage, isLanguageSelected }}>
      {children}
    </LanguageContext.Provider>
  );
};
