import { SamplingPoint, SamplingPointsOutputGeometry, SamplingPointsService } from 'api';
import { FilterResultEnum } from 'hooks/allFilters';
import { WaitTimeStateEnum } from 'utils/waitTime';

export type ServiceType = 'driveInTest' | 'normalTest' | 'expressTest';

export type SamplingPointWithGeometry = SamplingPoint & {
  geometry: SamplingPointsOutputGeometry;
};

export type Service = SamplingPointsService & { waitTimeState: WaitTimeStateEnum };
export type WeightedService = Service & { filterResult: FilterResultEnum };

export type DetailInfo = {
  pointId: number;
  serviceId: number;
};
