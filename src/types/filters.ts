import {
  SamplingPointsServiceProcessTypeEnum,
  SamplingPointsServiceQueueTypeEnum,
  SamplingPointsServiceRequestTypeEnum,
} from 'api';

export enum DistrictFilter {
  'PHA',
  'STC',
  'ANY',
}

export enum MethodFilter {
  'WALK_WITHOUT_RESERVATION',
  'IN_CAR',
  'WALK_WITH_RESERVATION',
}

export enum PriceFilter {
  'LOW',
  'HIGH',
  'ANY',
}

export enum CertificateFilter {
  'YES',
  'NO',
  'ANY',
}

export type changeFilterFn = <T extends keyof Filters>(name: T, value: Filters[T]) => void;

export type Filters = {
  districts: string[];
  serviceType?: SamplingPointsServiceRequestTypeEnum;
  reservationType?: SamplingPointsServiceQueueTypeEnum;
  samplingType?: SamplingPointsServiceProcessTypeEnum;
  selectedDate?: Date;
  hoursFrom: number;
  hoursTo: number;

  districtFilter: DistrictFilter; // TODO Delete
  serviceTypeFilter: SamplingPointsServiceRequestTypeEnum; // TODO Delete
  methodFilter: MethodFilter; // TODO Delete
  priceFilter: PriceFilter; // TODO Delete
  certificateFilter: CertificateFilter; // TODO Delete
};
