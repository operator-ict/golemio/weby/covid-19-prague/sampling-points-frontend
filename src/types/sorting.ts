export type TableColumns = {
  showPriceColumn: boolean;
  showClosestSlotColumn: boolean;
  showExpectedWaitTime: boolean;
};

export enum SortColumn {
  PRICE = 'PRICE',
  WAIT_TIME_OR_CLOSEST_SLOT = 'WAIT_TIME_OR_CLOSEST_SLOT',
  RESULT_TIME_HOURS = 'RESULT_TIME_HOURS',
}
