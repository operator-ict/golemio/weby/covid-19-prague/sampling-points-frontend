import { SamplingPointsServiceRequestTypeEnum } from 'api';
import { CertificateFilter, Filters, PriceFilter, MethodFilter } from 'types/filters';

export const getDerivedFilter = (prevFilters: Filters, filters: Filters): Filters => {
  const newFilters: Filters = { ...filters };

  // TODO: Consider removing?
  if (prevFilters.serviceTypeFilter !== filters.serviceTypeFilter) {
    if (filters.serviceTypeFilter === SamplingPointsServiceRequestTypeEnum.Application) {
      newFilters.certificateFilter = CertificateFilter.ANY;
      newFilters.priceFilter = PriceFilter.ANY;
    }
    if (filters.serviceTypeFilter === SamplingPointsServiceRequestTypeEnum.Selfpaid) {
      newFilters.certificateFilter = CertificateFilter.ANY;
      newFilters.priceFilter = PriceFilter.ANY;
    }
    if (
      filters.serviceTypeFilter === SamplingPointsServiceRequestTypeEnum.Agtests &&
      prevFilters.methodFilter === MethodFilter.WALK_WITHOUT_RESERVATION
    ) {
      newFilters.methodFilter = MethodFilter.WALK_WITH_RESERVATION;
    }
  }
  return newFilters;
};
